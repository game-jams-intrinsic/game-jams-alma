﻿using System;
using InputPlayerSystem;
using UnityEngine;
using UnityEngine.InputSystem;


namespace Utils.Input
{
    public class ReadInputPlayer : MonoBehaviour
    {
        private GameplayInput _playerInputActions;
        private Vector2 _movementInMenuInput;

        public event Action OnPlayerPressedSpaceKey = delegate { };
        public event Action OnPlayerStartMoving = delegate { };
        public event Action<Vector2> OnPlayerMovesMouse = delegate { };
        public event Action<Vector2> OnPlayerStopMovesMouse = delegate { };
        public event Action<float> OnPlayerPressVerticalAxisButtons = delegate { };
        public event Action<float> OnPlayerPressHorizontalAxisButtons = delegate { };


        public event Action<bool> OnPlayerPressLeftButtonMouse = delegate { };
        public event Action<bool> OnPlayerPressRightButtonMouse = delegate { };

        public event Action OnPlayerPressEnterButton = delegate { };
        public event Action OnPlayerPressGoThrowMirrorKey = delegate { };
        public event Action OnDebugEscPressed = delegate { };


        public Vector3 MovementInGameAxis { get; private set; }

        // public event Action OnEscapeButtonPressed = delegate { };
        private bool _isPause;

        public bool IsPause
        {
            get { return _isPause; }
        }

        private void OnEnable()
        {
            _playerInputActions?.Enable();
        }

        private void OnDisable()
        {
            _playerInputActions?.Disable();
        }

        private void Awake()
        {
            _playerInputActions = new GameplayInput();


            _playerInputActions.Gameplay.Interact.performed += ctx => InteractOneTime(); //PRESS ONE TIME
            // _playerInputActions.Gameplay.Interact.canceled += ctx => InteractReleased(); //PRESS AND RELEASE

            _playerInputActions.Gameplay.Movement.performed += PlayerMove;
            _playerInputActions.Gameplay.Movement.canceled += PlayerStopMoving;

            // _playerInputActions.Gameplay.AttackRight.performed += ctx => AttackRight();
            //TODO Check cuando dejamos de pulsar
            _playerInputActions.Gameplay.ClickItem.performed += ctx => UndoAttackRight();

            _playerInputActions.Gameplay.AttackLeft.performed += ctx => AttackLeft();
            _playerInputActions.Gameplay.AttackLeft.canceled += ctx => UndoAttackLeft();
            _playerInputActions.Gameplay.PauseGame.performed += ctx => EscapeButtonPressed();
            _playerInputActions.Gameplay.GoThrowMirror.performed += ctx => GoThrowMirrorPressed();
            
            _playerInputActions.Debug.EscapeKey.performed += ctx => DebugEscapeButtonPressed();


            _playerInputActions.Mouse.Rotation.performed += RotateMouse;
            _playerInputActions.Mouse.Rotation.canceled += StopRotateMouse;
        }

        private void GoThrowMirrorPressed()
        {
            OnPlayerPressGoThrowMirrorKey.Invoke();
        }

        private void StopRotateMouse(InputAction.CallbackContext obj)
        {
            Vector2 mousePosition = Vector2.zero;

            OnPlayerStopMovesMouse.Invoke(mousePosition);
        }

        private void RotateMouse(InputAction.CallbackContext obj)
        {
            Vector2 mousePosition = obj.ReadValue<Vector2>();
            OnPlayerMovesMouse.Invoke(mousePosition);
        }

        private void OnDestroy()
        {
            if (_playerInputActions != null)
            {
                _playerInputActions.Gameplay.Interact.performed -= ctx => InteractOneTime(); //PRESS ONE TIME
                _playerInputActions.Gameplay.Interact.canceled -= ctx => InteractReleased(); //PRESS AND RELEASE

                _playerInputActions.Gameplay.Movement.performed -= PlayerMove;
                _playerInputActions.Gameplay.Movement.canceled -= PlayerStopMoving;

                // _playerInputActions.Gameplay.AttackRight.performed += ctx => AttackRight();
                _playerInputActions.Gameplay.ClickItem.performed -= ctx => UndoAttackRight();

                _playerInputActions.Gameplay.AttackLeft.performed -= ctx => AttackLeft();
                _playerInputActions.Gameplay.AttackLeft.canceled -= ctx => UndoAttackLeft();

                _playerInputActions.Gameplay.PauseGame.performed -= ctx => EscapeButtonPressed();
                _playerInputActions.Debug.EscapeKey.performed -= ctx => DebugEscapeButtonPressed();
            }
        }

        private void DebugEscapeButtonPressed()
        {
            OnDebugEscPressed.Invoke();
        }



        private void EscapeButtonPressed()
        {
            OnDebugEscPressed.Invoke();
        }


        private void PlayerStopMoving(InputAction.CallbackContext obj)
        {
            this.MovementInGameAxis = Vector3.zero;
        }

        private void PlayerMove(InputAction.CallbackContext obj)
        {
            if (obj.ReadValue<Vector2>().magnitude > 0.01f)
            {
                Vector2 vectorReceived = obj.ReadValue<Vector2>();
                MovementInGameAxis = new Vector3(vectorReceived.x, 0, vectorReceived.y);
                OnPlayerStartMoving.Invoke();
            }
        }

        private void UndoAttackLeft()
        {
            OnPlayerPressLeftButtonMouse.Invoke(false);
        }

        private void UndoAttackRight()
        {
            OnPlayerPressRightButtonMouse.Invoke(false);
        }

        private void AttackLeft()
        {
            OnPlayerPressLeftButtonMouse.Invoke(true);
        }



        private void InteractOneTime()
        {
            Debug.Log("PRESS ONE TIME");

            OnPlayerPressedSpaceKey.Invoke();
        }

        private void InteractReleased()
        {
            Debug.Log("RELEASE");
        }

        public void DisableGameplayInput()
        {
            _playerInputActions.Gameplay.Disable();
        }

        public void EnableGameplayInput()
        {
            _playerInputActions.Gameplay.Enable();
        }
        
        public void DisableMouseInput()
        {
            _playerInputActions.Mouse.Disable();
        }

        public void EnableMouseInput()
        {
            _playerInputActions.Mouse.Enable();
        }
    }
}