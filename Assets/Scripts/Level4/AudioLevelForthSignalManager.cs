﻿using Signals.EventManager;
using UnityEngine;
using Utils;

public class AudioLevelForthSignalManager : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSourceWhenPlayerEnterThirdPartSoundFirst,
        _audioWhenPlayerEnterThirdPartSoundSecond,
        _audioSourceBackgroundSoundLevelZero;

    private IEventManager _eventManager;
    private Timer _timer;

    void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    private void Start()
    {
        _timer = new Timer();
        _eventManager.AddActionToSignal<MuteAllAudiosBeforeExitLevelZeroSignal>(MuteAllAudios);
        _eventManager.AddActionToSignal<PlayWhenPlayerEnterThirdPartSoundSignal>(WhenPlayerEnterThirdPartSound);
        _audioSourceBackgroundSoundLevelZero.Play();
    }

    private void WhenPlayerEnterThirdPartSound(PlayWhenPlayerEnterThirdPartSoundSignal obj)
    {
        _audioSourceWhenPlayerEnterThirdPartSoundFirst.Play();
        _timer.OnTimerEnds += PlayPlayerEnterThirdRoomSecondPartSound;
        _timer.SetTimeToWait(_audioSourceWhenPlayerEnterThirdPartSoundFirst.clip.length);
        StartCoroutine(_timer.TimerCoroutine());
    }

    private void PlayPlayerEnterThirdRoomSecondPartSound()
    {
        _timer.OnTimerEnds -= PlayPlayerEnterThirdRoomSecondPartSound;
        _audioWhenPlayerEnterThirdPartSoundSecond.Play();
        new StartMovementCameraAfterEnterCenterZoneSignal().Execute();
    }


    private void MuteAllAudios(MuteAllAudiosBeforeExitLevelZeroSignal obj)
    {
        _audioSourceBackgroundSoundLevelZero.Stop();
    }
}