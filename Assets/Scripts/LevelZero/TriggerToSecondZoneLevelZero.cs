﻿using UnityEngine;

public class TriggerToSecondZoneLevelZero : MonoBehaviour
{
    [SerializeField] private ColliderPassingChecker _colliderPassingChecker;
    [SerializeField] private Transform _positionToResetPlayer;

    private bool _hasCrossedOnce = false;

    private void Start()
    {
        _colliderPassingChecker.OnPlayerPassCollider += PlayerPassCollider;
    }

    private void PlayerPassCollider(Transform playerTransform)
    {
        if (!_hasCrossedOnce)
        {
            new StartSwitcherFirstLightlevelZeroSignal().Execute();
            new StartSwitcherSecondLightSignal().Execute();
            new PlayPlayerReachGreyZoneSoundSignal().Execute();
            _hasCrossedOnce = true;
        }
        else
        {
            playerTransform.position = _positionToResetPlayer.position;
        }
    }
}