﻿using Signals.EventManager;
using UnityEngine;
using Utils;

public class LightManagerLevelZero : MonoBehaviour
{
    // [SerializeField] private float _initialIntensityLightOne;

    [SerializeField] private float _timeToSwitchOffFirstLight = 5, _timeToSwitchOnSecondLight = 5;

    [SerializeField] private Light _firstLight,
        _secondLight,
        _thirdLightCenter1,
        _thirdLightCenter2,
        _thirdLightRightDoor,
        _thirdLightLefttDoor;
    private IEventManager _eventManager;
    private SwitcherLight _switcherFirstLight, _switcherSecondLight;

    private void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    void Start()
    {
        _thirdLightCenter1.enabled = false;
        _thirdLightCenter2.enabled = false;
        _thirdLightRightDoor.enabled = false;
        _thirdLightLefttDoor.enabled = false;
        _firstLight.enabled = false;
        _secondLight.enabled = false;
        _eventManager.AddActionToSignal<EnableFirstLightSignal>(EnableFirstLight);
        _eventManager.AddActionToSignal<EnableLightCenterThirdPartSignal>(EnableThirdLight);
        _eventManager.AddActionToSignal<EnableLightRightDoorThirdPartSignal>(EnableRightThirdDoorLight);
        _eventManager.AddActionToSignal<EnableLightLeftDoorThirdPartSignal>(EnableLeftThirdDoorLight);
        _eventManager.AddActionToSignal<DisableSecondLightSignal>(DisableSecondLight);

        _eventManager.AddActionToSignal<StartSwitcherFirstLightlevelZeroSignal>(StartSwitcherLightFirst);
        _eventManager.AddActionToSignal<StartSwitcherSecondLightSignal>(StartSwitcherSecondLight);

        _switcherFirstLight = new SwitcherLight(_firstLight, _timeToSwitchOffFirstLight, true, 0);
        _switcherSecondLight = new SwitcherLight(_secondLight, _timeToSwitchOnSecondLight, false, _secondLight.intensity);
    }

    private void OnDestroy()
    {
        _eventManager?.RemoveActionFromSignal<EnableFirstLightSignal>(EnableFirstLight);
        _eventManager?.RemoveActionFromSignal<EnableLightCenterThirdPartSignal>(EnableThirdLight);
        _eventManager?.RemoveActionFromSignal<EnableLightRightDoorThirdPartSignal>(EnableRightThirdDoorLight);
        _eventManager?.RemoveActionFromSignal<EnableLightLeftDoorThirdPartSignal>(EnableLeftThirdDoorLight);
        _eventManager?.RemoveActionFromSignal<DisableSecondLightSignal>(DisableSecondLight);

        _eventManager?.RemoveActionFromSignal<StartSwitcherFirstLightlevelZeroSignal>(StartSwitcherLightFirst);
        _eventManager?.RemoveActionFromSignal<StartSwitcherSecondLightSignal>(StartSwitcherSecondLight);
    }

    private void StartSwitcherLightFirst(StartSwitcherFirstLightlevelZeroSignal obj)
    {
        StartCoroutine(_switcherFirstLight.Start());
    }

    private void StartSwitcherSecondLight(StartSwitcherSecondLightSignal obj)
    {
        _secondLight.enabled = true;
        StartCoroutine(_switcherSecondLight.Start());
    }

    private void DisableSecondLight(DisableSecondLightSignal obj)
    {
        _secondLight.enabled = false;
    }

    private void EnableLeftThirdDoorLight(EnableLightLeftDoorThirdPartSignal obj)
    {
        _thirdLightLefttDoor.enabled = true;
    }

    private void EnableRightThirdDoorLight(EnableLightRightDoorThirdPartSignal obj)
    {
        _thirdLightRightDoor.enabled = true;
    }

    private void EnableThirdLight(EnableLightCenterThirdPartSignal obj)
    {
        _thirdLightCenter1.enabled = true;
        _thirdLightCenter2.enabled = true;
    }

    private void EnableFirstLight(EnableFirstLightSignal signal)
    {
        _firstLight.enabled = true;
    }
}