﻿using UnityEngine;
using Utils;
using Utils.Input;

public class CanvasInitLevelPresenterLevelZero : MonoBehaviour
{
    [SerializeField] private CanvasGroup _canvasGroupWasd;
    [SerializeField] private TextWritter _textWritter;
    private ReadInputPlayer _readInputPlayer;
    private bool _isHidden = false;
    private void Awake()
    {
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
    }

    void Start()
    {
        _canvasGroupWasd.gameObject.SetActive(false);
        _readInputPlayer.OnDebugEscPressed += PassInitText;
        _readInputPlayer.OnPlayerStartMoving += HideWASDImage;
        _textWritter.OnTextHasEnd += InitialTextHasEnd;
    }
    
    private void OnDestroy()
    {
        _readInputPlayer.OnDebugEscPressed -= PassInitText;
        _textWritter.OnTextHasEnd -= InitialTextHasEnd;
        _readInputPlayer.OnPlayerStartMoving -= HideWASDImage;

    }

    private void InitialTextHasEnd()
    {
        ShowWASDImage();
        EnablePlayerMovement();
        EnableFirstLight();
        new EnableMovementAfterEventEndedSignal().Execute();
    }

    private void EnableFirstLight()
    {
        new EnableFirstLightSignal().Execute();
    }

    private void HideWASDImage()
    {
        if (_isHidden)
        {
            return;
        }

        _canvasGroupWasd.gameObject.SetActive(false);
        new PlayPlayerGoToGreyZoneSoundSignal().Execute();
        _isHidden = true;
    }

    private void PassInitText()
    {
        if (!_textWritter.ShowingText) return;
        _textWritter.TextHasEnd();
        _textWritter.gameObject.SetActive(false);
    }

    private void ShowWASDImage()
    {
        _canvasGroupWasd.gameObject.SetActive(true);
    }

    private void EnablePlayerMovement()
    {
        new EnablePlayerMovementSignal().Execute();
    }
}