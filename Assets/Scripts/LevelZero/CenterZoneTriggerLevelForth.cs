﻿using UnityEngine;

public class CenterZoneTriggerLevelForth : MonoBehaviour
{
    [SerializeField] private Transform _positionToMovePlayer;
    private bool _hasPast;
    
    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("Player") || _hasPast) return;
        _hasPast = true;
        other.transform.position = _positionToMovePlayer.position;
        new DisablePlayerMovementSignal().Execute();
        new DisableMouseMovementSignal().Execute();
        new ResetCameraRotationSignal().Execute();
        new PlayWhenPlayerEnterThirdPartSoundSignal().Execute();
    }
}