﻿using Signals.EventManager;
using UnityEngine;
using Utils;

public class AudioLevelZeroSignalManager : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSourceWhenPlayerGoToGreyLightSound,
        _audioWhenPlayerReachToGreyZone,
        _audioSourceBackgroundSoundLevelZero,
        _audioSourceReachCenterZoneSoundLevelZero,
        _audioSourceWhenTriggerLockedDoorSound;

    private IEventManager _eventManager;

    void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    private void Start()
    {
        _eventManager.AddActionToSignal<PlayPlayerGoToGreyZoneSoundSignal>(PlayPlayerGoToGreyZoneSound);
        _eventManager.AddActionToSignal<PlayPlayerReachGreyZoneSoundSignal>(PlayPlayerReachGreyZoneSound);
        _eventManager.AddActionToSignal<MuteAllAudiosBeforeExitLevelZeroSignal>(MuteAllAudios);
        _eventManager.AddActionToSignal<PlayPlayerReachCenterSoundSignal>(PlayPlayerReachCenterSound);
        _eventManager.AddActionToSignal<PlayWhenTriggerLockedDoorSoundSignal>(PlayWhenTriggerLockedDoorSound);
        _audioSourceBackgroundSoundLevelZero.Play();
    }

    private void PlayWhenTriggerLockedDoorSound(PlayWhenTriggerLockedDoorSoundSignal obj)
    {
        _audioSourceWhenTriggerLockedDoorSound.Play();
    }

    private void PlayPlayerReachGreyZoneSound(PlayPlayerReachGreyZoneSoundSignal obj)
    {
        _audioWhenPlayerReachToGreyZone.Play();
    }

    private void PlayPlayerReachCenterSound(PlayPlayerReachCenterSoundSignal obj)
    {
        _audioSourceReachCenterZoneSoundLevelZero.Play();
    }

    private void MuteAllAudios(MuteAllAudiosBeforeExitLevelZeroSignal obj)
    {
        _audioSourceBackgroundSoundLevelZero.Stop();
    }

    private void PlayPlayerGoToGreyZoneSound(PlayPlayerGoToGreyZoneSoundSignal obj)
    {
        _audioSourceWhenPlayerGoToGreyLightSound.Play();
    }
}