﻿using UnityEngine;

public class TriggerToSecondZoneLevelForth : MonoBehaviour
{
    [SerializeField] private ColliderPassingChecker _colliderPassingChecker;

    private bool _hasCrossedOnce = false;

    private void Start()
    {
        _colliderPassingChecker.OnPlayerPassCollider += PlayerPassCollider;
    }

    private void PlayerPassCollider(Transform playerTransform)
    {
        if (_hasCrossedOnce) return;
        new StartSwitcherFirstLightlevelZeroSignal().Execute();
        new StartSwitcherSecondLightSignal().Execute();
        _hasCrossedOnce = true;
        
    }
}