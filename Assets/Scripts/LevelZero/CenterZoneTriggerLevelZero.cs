﻿using UnityEngine;

public class CenterZoneTriggerLevelZero : MonoBehaviour
{
    [SerializeField]private float _timeToShowDoorLights;
    private Timer _timer;
    private bool _hasPast;

    [SerializeField] private Transform _positionToMovePlayer;

    void Start()
    {
        _timer = new Timer();
        _timer.SetTimeToWait(_timeToShowDoorLights);
        _timer.OnTimerEnds += StartMovementOfCameraToShowDoors;
    }

    private void StartMovementOfCameraToShowDoors()
    {
        _timer.OnTimerEnds -= StartMovementOfCameraToShowDoors;

        new StartMovementCameraAfterEnterCenterZoneSignal().Execute();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("Player") || _hasPast) return;
        _hasPast = true;
        other.transform.position = _positionToMovePlayer.position;
        new DisablePlayerMovementSignal().Execute();
        new DisableMouseMovementSignal().Execute();
        new ResetCameraRotationSignal().Execute();
        new PlayPlayerReachCenterSoundSignal().Execute();
        StartCoroutine(_timer.TimerCoroutine());
    }
}