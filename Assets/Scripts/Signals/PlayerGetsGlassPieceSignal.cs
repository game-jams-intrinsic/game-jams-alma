﻿using Signals;

public class  PlayerGetsGlassPieceSignal : SignalBase
{
    public int PieceOfGlass { get; set; }
}