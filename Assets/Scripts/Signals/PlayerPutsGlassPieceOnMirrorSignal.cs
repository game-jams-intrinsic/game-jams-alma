﻿using Signals;

public class PlayerPutsGlassPieceOnMirrorSignal : SignalBase
{
    public int GlassPieceId;
}