﻿using Signals;

public class ShowTextOnCanvasSignal : SignalBase
{
    public string TextToShow;
}