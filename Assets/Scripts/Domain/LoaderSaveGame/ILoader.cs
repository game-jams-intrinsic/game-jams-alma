﻿namespace Domain.LoaderSaveGame
{
    public interface ILoader
    {
        GameDataPOCO LoadGame();
        bool HasSavedGame();
    }
}