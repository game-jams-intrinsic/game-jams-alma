﻿using Domain.JsonTranslator;
using UnityEngine;
using Utils;

namespace Domain.LoaderSaveGame
{
    public class LoadWithPlayerPrefs : ILoader
    {
        private IJsonator _jsonator;

        public LoadWithPlayerPrefs()
        {
            _jsonator = ServiceLocator.Instance.GetService<IJsonator>();
        }

        public GameDataPOCO LoadGame()
        {
            if (!PlayerPrefs.HasKey("SaveGame"))
            {
                return null;
            }

            var dataToFromJson = PlayerPrefs.GetString("SaveGame");
            GameDataPOCO gameDataJson = _jsonator.FromJson<GameDataPOCO>(dataToFromJson);
            return gameDataJson;
        }

        public bool HasSavedGame()
        {
            return PlayerPrefs.GetInt("HasSavedGame") == 1;
        }
    }
}