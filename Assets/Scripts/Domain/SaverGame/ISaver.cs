﻿namespace Domain.SaverGame
{
    public interface ISaver
    {
        void SaveGame(GameDataPOCO gameData);
        void DeleteSaveGame();
        void SaveNewGameStatus(bool statusToSave);
    }
}