﻿using System;

namespace App.GameData
{
    [Serializable]
    public class GameDataInfo
    {
        public int SavePointId;
        public int MaximumEnergyVessels;
        public float MaximumLife;
        public float CurrentLife;

        public GameDataInfo()
        {
            SavePointId = -1;
        }
    }
}