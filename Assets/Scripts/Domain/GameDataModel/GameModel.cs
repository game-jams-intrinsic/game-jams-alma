﻿using App.GameData;
using App.PlayerModelInfo;
using Domain;
using Domain.LoaderSaveGame;
using Domain.SaverGame;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;

namespace App.GameDataModel
{
    public class GameModel : IGameModel
    {
        private ISaver _saver;
        private ILoader _loader;
        private IPlayerModel _playerModel;

        public GameModel()
        {
            _saver = ServiceLocator.Instance.GetService<ISaver>();
            _loader = ServiceLocator.Instance.GetService<ILoader>();
            _playerModel = ServiceLocator.Instance.GetModel<IPlayerModel>();
        }
        

        public void PauseGame()
        {
            Time.timeScale = 0;
        }

        public void ContinueGame()
        {
            Time.timeScale = 1;
        }

        public void RestartScene()
        {
           
            ServiceLocator.Instance.GetModel<IPlayerModel>().ResetData();
            ContinueGame();
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void LoadInitScene()
        {
            SceneManager.LoadScene("InitScene");
        }

        public void QuitGame()
        {
            Application.Quit();
        }

        public void SaveGame(int currentIdSavePoint)
        {
            GameDataPOCO gameData = new GameDataPOCO()
            {

            };
            _saver.SaveGame(gameData);
            Debug.Log("Saved");
        }

        public GameDataInfo LoadGame()
        {
            GameDataPOCO poco = _loader.LoadGame();
            return new GameDataInfo()
            {
                CurrentLife = poco.CurrentLife,
                MaximumEnergyVessels = poco.MaximumEnergyVessels,
                MaximumLife = poco.MaximumLife,
                SavePointId = poco.SavePointId
            };
        }

        public bool HasSavedGame()
        {
            return _loader.HasSavedGame();
        }

        public void SetNewGameStatus(bool statusToSave)
        {
            _saver.SaveNewGameStatus(statusToSave);
        }

        public void DeleteLastSaveGame()
        {
            _saver.DeleteSaveGame();
        }
    }
}