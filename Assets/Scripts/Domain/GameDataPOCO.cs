﻿namespace Domain
{
    public class GameDataPOCO
    {
        public int SavePointId;
        public int MaximumEnergyVessels;
        public float MaximumLife;
        public float CurrentLife;

        public GameDataPOCO()
        {
            SavePointId = -1;
        }
    }
}