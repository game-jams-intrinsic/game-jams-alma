﻿using App.PlayerModelInfo;
using Presentation.Player;
using Signals.EventManager;
using UnityEngine;
using Utils;
using Utils.Input;

namespace Presentation.Installers
{
    public class InstallerInGamePresentation : MonoBehaviour
    {
        // [SerializeField] private bool _enable;
        private GameObject _readInputPlayerPrefab, _readInputPlayerInstance;
        private GameObject _languageManagerPrefab, _languageManagerInstance;

        private void Awake()
        {
            InstallerInGamePresentation[] installers = FindObjectsOfType<InstallerInGamePresentation>();
            // if (!_enable){
            //     return;
            // }

            _readInputPlayerPrefab = Resources.Load("Prefabs/Common/Managers/ReadInputPlayer") as GameObject;
            _languageManagerPrefab = Resources.Load("Prefabs/Common/Managers/LanguageManager") as GameObject;
            _readInputPlayerInstance = Instantiate(_readInputPlayerPrefab);
            _languageManagerInstance = Instantiate(_languageManagerPrefab);

            ServiceLocator.Instance.RegisterModel<IPlayerModel>(new PlayerModel());
            ServiceLocator.Instance.RegisterModel<IEndLevelModel>(new EndLevelModel());
            ServiceLocator.Instance.RegisterService<IEventManager>(new EventManager());
            ServiceLocator.Instance.RegisterService(_languageManagerInstance
                .GetComponent<ILanguageManager>());
            ServiceLocator.Instance.RegisterService(_readInputPlayerInstance.GetComponent<ReadInputPlayer>());

            _languageManagerInstance
                .GetComponent<ILanguageManager>().SetActualLanguageText(LanguagesKeys.SPA);

            // DontDestroyOnLoad(this);
        }

        private void OnDestroy()
        {
            ServiceLocator.Instance.UnregisterService<ReadInputPlayer>();
            ServiceLocator.Instance.UnregisterService<IEventManager>();
            ServiceLocator.Instance.UnregisterService<ILanguageManager>();
        }
    }
}