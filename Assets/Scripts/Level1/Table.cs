﻿using UnityEngine;

public class Table : MonoBehaviour
{
    [SerializeField] private Vase _vase;
    private bool _hasTriggered;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && !_hasTriggered)
        {
            _vase.MoveToLeft();
            _hasTriggered = !_hasTriggered;
        }
    }
}