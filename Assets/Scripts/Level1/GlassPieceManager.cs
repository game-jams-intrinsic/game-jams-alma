﻿using System.Collections.Generic;
using Signals.EventManager;
using UnityEngine;
using Utils;

public abstract class GlassPieceManager : MonoBehaviour
{
    public List<GlassInitInfoLevelOne> _glassPieces;

    protected IEventManager _eventManager;
    protected int _idGlassEnabledAtFirst;
    protected int _glassPiecesRecovered;

    public List<GlassInitInfoLevelOne> GlassPieces
    {
        get => _glassPieces;
        set => _glassPieces = value;
    }

    private void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    protected void Start()
    {
        _glassPiecesRecovered = 0;
        _eventManager.AddActionToSignal<PlayerGetsGlassPieceSignal>(PlayerGetsGlassPiece);
        _eventManager.AddActionToSignal<PlayerPutsGlassPieceOnMirrorSignal>(PlayerHasPutGlassPieceOnMirror);
    }

    protected virtual void PlayerGetsGlassPiece(PlayerGetsGlassPieceSignal obj)
    {
        
    }
    
    protected virtual void PlayerHasPutGlassPieceOnMirror(PlayerPutsGlassPieceOnMirrorSignal obj)
    {
        _glassPiecesRecovered++;
        new HideGlassPieceOfCanvasSignal().Execute();
        new PlayGlassPiecesIsPutInMirrorAudioSignal().Execute();
    }
}