﻿using UnityEngine;

public class TriggerAudioBathroom : MonoBehaviour
{
    private bool _hasTriggered;
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player") || _hasTriggered)
        {
            return;
        }
        _hasTriggered = true;
        new PlayWhenPlayerEntersBathRoomSignal().Execute();
    }
}