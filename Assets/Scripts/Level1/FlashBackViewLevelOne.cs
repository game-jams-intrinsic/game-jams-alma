﻿using UnityEngine;

public class FlashBackViewLevelOne : FlashBackViewLevel
{
    [SerializeField] private Sprite _spriteVaseFlashback1,
        _spriteVaseFlashback2,
        _spriteFridgeFlashback1,
        _spriteFridgeFlashback2;

    private int _flashBackAfterCompleteMirrorIndex = 0;

    public void ShowFlashBlackVase()
    {
        EnableStructureToShowFlashbacks();
        ShowFirstFlashBackVase();
    }

    public void ShowFlashBlackFridge()
    {
        EnableStructureToShowFlashbacks();
        ShowFirstFlashBackFridge();
    }

    public void ShowFlashWhenMirrorIsCompleted()
    {
        EnableStructureToShowFlashbacks();
        ShowSpritesOfListSpritesMirrorIsCompleted();
    }

    private void ShowSpritesOfListSpritesMirrorIsCompleted()
    {
        _timer.OnTimerEnds -= ShowSpritesOfListSpritesMirrorIsCompleted;
        if (_flashBackAfterCompleteMirrorIndex >= _flashbacksToShowAfterCompleteMirror.Count)
        {
            _timer.OnTimerEnds -= ShowSpritesOfListSpritesMirrorIsCompleted;
            RemoveFlashBack();
            new ShakeCameraSignal()
            {
                ActionToDoAfter = _actionToDoAfterCameraShakeWhenPlayerCompletesMirror,
                TimeToShake = 7
            }.Execute();

            return;
        }

        _spriteRenderer.sprite = _flashbacksToShowAfterCompleteMirror[_flashBackAfterCompleteMirrorIndex];
        _timer.SetTimeToWait(_timeToShowSprites);
        _timer.OnTimerEnds += ShowSpritesOfListSpritesMirrorIsCompleted;
        StartCoroutine(_timer.TimerCoroutine());
        _flashBackAfterCompleteMirrorIndex++;
    }
    
    private void ShowFirstFlashBackFridge()
    {
        _spriteRenderer.sprite = _spriteFridgeFlashback1;
        _timer.SetTimeToWait(_timeToShowSprites);
        _timer.OnTimerEnds += ShowSecondFlashbackFridge;
        StartCoroutine(_timer.TimerCoroutine());
    }

    private void ShowSecondFlashbackFridge()
    {
        _timer.OnTimerEnds -= ShowSecondFlashbackFridge;
        _spriteRenderer.sprite = _spriteFridgeFlashback2;
        _timer.SetTimeToWait(_timeToShowSprites);
        _timer.OnTimerEnds += RemoveFlashBack;
        StartCoroutine(_timer.TimerCoroutine());
    }

    private void ShowFirstFlashBackVase()
    {
        _spriteRenderer.sprite = _spriteVaseFlashback1;
        _timer.SetTimeToWait(_timeToShowSprites);
        _timer.OnTimerEnds += ShowSecondFlashback;
        StartCoroutine(_timer.TimerCoroutine());
    }

    private void ShowSecondFlashback()
    {
        _timer.OnTimerEnds -= ShowSecondFlashback;
        _spriteRenderer.sprite = _spriteVaseFlashback2;
        _timer.SetTimeToWait(_timeToShowSprites);
        StartCoroutine(_timer.TimerCoroutine());
        _timer.OnTimerEnds += RemoveFlashBack;
    }
}