﻿using UnityEngine;

public class InitialActionsLevelZeroUseCase : MonoBehaviour
{
    void Start()
    {
        new DisablePlayerMovementSignal().Execute();
        new DisableMovementAfterEventStartedSignal().Execute();
    }
}