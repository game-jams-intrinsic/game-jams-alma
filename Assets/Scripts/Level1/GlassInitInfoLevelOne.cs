﻿using System;

[Serializable]
public class GlassInitInfoLevelOne : GlassInitInfo
{
    public LocalizationsLevelOne Localization;
}