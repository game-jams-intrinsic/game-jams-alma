﻿public class CanvasLevelPresenterLevelOne : CanvasLevelPresenter
{
    private FlashBackViewLevelOne _flashBackViewLevelOne;

    private void Awake()
    {
        base.Awake();
        _flashBackViewLevelOne = _flashBackViewLevel.GetComponent<FlashBackViewLevelOne>();
    }

    private void Start()
    {
        base.Start();

        _eventManager.AddActionToSignal<ShowVaseFlashBacksSignal>(ShowFlashBacksVase);
        _eventManager.AddActionToSignal<ShowFlashbacksFridgeSignal>(ShowFlashBacksFridge);
        _eventManager.AddActionToSignal<ShowFlashBackWhenMirrorIsCompletedLevelOneSignal>(
            ShowFlashBackWhenMirrorIsCompleted);
    }

    private void OnDestroy()
    {
        base.OnDestroy();
        _eventManager.RemoveActionFromSignal<ShowVaseFlashBacksSignal>(ShowFlashBacksVase);
        _eventManager.RemoveActionFromSignal<ShowFlashbacksFridgeSignal>(ShowFlashBacksFridge);
        _eventManager.RemoveActionFromSignal<ShowFlashBackWhenMirrorIsCompletedLevelOneSignal>(
            ShowFlashBackWhenMirrorIsCompleted);
    }

    private void ShowFlashBackWhenMirrorIsCompleted(ShowFlashBackWhenMirrorIsCompletedLevelOneSignal obj)
    {
        new ShowFlashBackOrImagesSignal().Execute();
        new ShowOnlyUIMaskSignal().Execute();
        HideGlassPiece();
        _flashBackViewLevelOne.gameObject.SetActive(true);
        _flashBackViewLevelOne.ShowFlashWhenMirrorIsCompleted();
    }

    private void ShowFlashBacksFridge(ShowFlashbacksFridgeSignal obj)
    {
        new ShowFlashBackOrImagesSignal().Execute();

        new ShowOnlyUIMaskSignal().Execute();
        HideGlassPiece();
        _flashBackViewLevel.gameObject.SetActive(true);
        _flashBackViewLevelOne.ShowFlashBlackFridge();
    }

    private void ShowFlashBacksVase(ShowVaseFlashBacksSignal obj)
    {
        new ShowFlashBackOrImagesSignal().Execute();

        new ShowOnlyUIMaskSignal().Execute();
        HideGlassPiece();
        _flashBackViewLevelOne.gameObject.SetActive(true);
        _flashBackViewLevelOne.ShowFlashBlackVase();
    }
}