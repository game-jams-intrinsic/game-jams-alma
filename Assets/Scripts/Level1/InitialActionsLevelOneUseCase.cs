﻿using UnityEngine;

public class InitialActionsLevelOneUseCase : MonoBehaviour
{
    void Start()
    {
        new DisablePlayerMovementSignal().Execute();
        new PlayBackgroundMusicLevelOneSignal().Execute();
        new PlayHouseBackGroundMusicLevelOneSignal().Execute();
        new SwitchOnAllLightsLevelOneSignal().Execute();
        new DisableMovementAfterEventStartedSignal().Execute();
    }
}