﻿using UnityEngine;
using Utils;

public class Vase : MonoBehaviour
{
    [SerializeField] public float _valueToMove;
    [SerializeField] public float _speedMovement;
    [SerializeField] private ActionToDo _actionToDo;
    private bool _hasBeenTriggered;

    private void OnCollisionEnter(Collision other)
    {
        if (_hasBeenTriggered)
        {
            return;
        }

        if (other.collider.gameObject.layer == LayerMask.NameToLayer("Floor"))
        {
            _actionToDo.ExecuteAction();
            Debug.Log("Toca suelo");
            _hasBeenTriggered = true;
        }
    }

    public void MoveToLeft()
    {
        Vector3 destination = transform.position + Vector3.forward * _valueToMove;
        MoveCoroutine moveCoroutine = new MoveCoroutine(transform, transform.position,
            destination, _speedMovement);
        StartCoroutine(moveCoroutine.Start());
    }
}