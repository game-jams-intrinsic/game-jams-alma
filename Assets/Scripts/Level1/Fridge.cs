﻿using UnityEngine;

public class Fridge : MonoBehaviour
{
    [SerializeField] public ActionToDo _actionToDoAfterShake;
    private bool _hasEnteredBefore = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && !_hasEnteredBefore)
        {
            new ShakeCameraSignal() {TimeToShake = 4.0f, ActionToDoAfter = _actionToDoAfterShake}.Execute();
            _hasEnteredBefore = true;
        }
    }
}