﻿using Presentation.Player;
using Signals.EventManager;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;
using Utils.Input;

public class AudioManagerLevelOne : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSourceBackGround;
    [SerializeField] private AudioSource _audioSourceConversationAtStartLevel;
    [SerializeField] private AudioSource _audioSourceHouseBackgroundSound;
    [SerializeField] private AudioSource _audioSourcePlayWhenPlayerEntersBathRoom;
    [SerializeField] private AudioSource _audioSourceWhenPlayerGetsGlassPieceBathroom;
    [SerializeField] private AudioSource _audioSourceWhenPlayerGetsGlassPieceBedroom;
    [SerializeField] private AudioSource _audioSourceConversationWhenPlayerPutBathroomMirrorPiece;
    [SerializeField] private AudioSource _audioSourceStarterConversationWhenPlayerGetsPicture;
    [SerializeField] private AudioSource _audioSourceWhenVaseHitsGround;
    [SerializeField] private AudioSource _audioSourceWhenPlayerTriggersFridge;
    [SerializeField] private AudioSource _audioSourceWhenMirrorIsCompleted;
    [SerializeField] private AudioSource _audioSourceWhenStartsVaseFlashback;
    [SerializeField] private AudioSource _audioSourceWhenPlayerPutGlassPieceInMirror;
    [SerializeField] private AudioSource _audioSourceWhenPlayerTriggerLockedDoorSound;
    
    
    private IEventManager _eventManager;
    private Timer _timer;
    private ReadInputPlayer _readInputPlayer;
    private IEndLevelModel _endLevelModel;

    private bool _firstAudioIsPlaying;
    private void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
        _endLevelModel = ServiceLocator.Instance.GetModel<IEndLevelModel>();
        _timer = new Timer();
        _eventManager.AddActionToSignal<PlayBackgroundMusicLevelOneSignal>(PlayBackGroundLevel);
        _eventManager.AddActionToSignal<DisableMovementAfterEventStartedSignal>(PlayConversationAtStartLevel);
        _eventManager.AddActionToSignal<PlayHouseBackGroundMusicLevelOneSignal>(PlayHouseBackGroundMusicLevelOne);
        _eventManager.AddActionToSignal<PlayAudioAfterPickingBathRoomLevelOneGlassSignal>(PlayAudioAfterPickingBathRoomGlass);
        _eventManager.AddActionToSignal<PlayAudioAfterPickingBedroomLevelOneGlassSignal>(
            PlayFirstAudioAfterPickingBedroomGlass);
        _eventManager.AddActionToSignal<PlayVaseHitsGroundSignal>(PlayAudioVaseHitsGround);
        _eventManager.AddActionToSignal<PlayAudioAfterPlayerTriggersFridgeSignal>(PlayAudioAfterTriggeredFridge);
        _eventManager.AddActionToSignal<PlayAudioMirrorIsCompletedSignal>(PlayAudioMirrorIsCompleted);
        _eventManager.AddActionToSignal<PlaySoundFlashBackVaseSignal>(PlayAudioStartFlashBackVase);
        _eventManager.AddActionToSignal<PlayWhenPlayerEntersBathRoomSignal>(PlayWhenPlayerEntersBathRoom);
        _eventManager.AddActionToSignal<PlayAudioAfterTakingMotherPictureSignal>(PlayFirstAudioAfterTakingPicture);
        _eventManager.AddActionToSignal<PlayConversationWhenPlayerHasPutFirstMirrorPieceSignal>(
            StartConversationWhenPlayerHasPutFirstMirrorPiece);
        _eventManager.AddActionToSignal<PlayGlassPiecesIsPutInMirrorAudioSignal>(PlayGlassPiecesIsPutInMirrorAudio);
        _eventManager.AddActionToSignal<PlayWhenTriggerLockedDoorSoundSignal>(PlayWhenTriggerLockedDoorSound);

        
        Debug.Log(_audioSourceWhenMirrorIsCompleted.clip.length);
        
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
        // _readInputPlayer.OnDebugEscPressed += PauseStartAudio;

    }


    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<PlayBackgroundMusicLevelOneSignal>(PlayBackGroundLevel);
        _eventManager.RemoveActionFromSignal<DisableMovementAfterEventStartedSignal>(PlayConversationAtStartLevel);
        _eventManager.RemoveActionFromSignal<PlayHouseBackGroundMusicLevelOneSignal>(PlayHouseBackGroundMusicLevelOne);
        _eventManager.RemoveActionFromSignal<PlayAudioAfterPickingBathRoomLevelOneGlassSignal>(PlayAudioAfterPickingBathRoomGlass);
        _eventManager.RemoveActionFromSignal<PlayAudioAfterPickingBedroomLevelOneGlassSignal>(
            PlayFirstAudioAfterPickingBedroomGlass);
        _eventManager.RemoveActionFromSignal<PlayVaseHitsGroundSignal>(PlayAudioVaseHitsGround);
        _eventManager.RemoveActionFromSignal<PlayAudioAfterPlayerTriggersFridgeSignal>(PlayAudioAfterTriggeredFridge);
        _eventManager.RemoveActionFromSignal<PlayAudioMirrorIsCompletedSignal>(PlayAudioMirrorIsCompleted);
        _eventManager.RemoveActionFromSignal<PlaySoundFlashBackVaseSignal>(PlayAudioStartFlashBackVase);
        _eventManager.RemoveActionFromSignal<PlayWhenPlayerEntersBathRoomSignal>(PlayWhenPlayerEntersBathRoom);
        _eventManager.RemoveActionFromSignal<PlayAudioAfterTakingMotherPictureSignal>(PlayFirstAudioAfterTakingPicture);
        _eventManager.RemoveActionFromSignal<PlayConversationWhenPlayerHasPutFirstMirrorPieceSignal>(
            StartConversationWhenPlayerHasPutFirstMirrorPiece);
        _eventManager.RemoveActionFromSignal<PlayGlassPiecesIsPutInMirrorAudioSignal>(PlayGlassPiecesIsPutInMirrorAudio);
        _eventManager.RemoveActionFromSignal<PlayWhenTriggerLockedDoorSoundSignal>(PlayWhenTriggerLockedDoorSound);
    }
    
    private void PlayWhenTriggerLockedDoorSound(PlayWhenTriggerLockedDoorSoundSignal obj)
    {
        _audioSourceWhenPlayerTriggerLockedDoorSound.Play();
    }

    
    private void PlayGlassPiecesIsPutInMirrorAudio(PlayGlassPiecesIsPutInMirrorAudioSignal obj)
    {
        _audioSourceWhenPlayerPutGlassPieceInMirror.Play();
    }
    
    private void PauseStartAudio()
    {
        if (!_firstAudioIsPlaying)
        {
            return;
        }

        _firstAudioIsPlaying = false;
        _audioSourceConversationAtStartLevel.Stop();

        new EnablePlayerMovementSignal().Execute();
    }

    private void PlayAudioStartFlashBackVase(PlaySoundFlashBackVaseSignal obj)
    {
        _audioSourceWhenStartsVaseFlashback.Play();
    }

    private void PlayAudioMirrorIsCompleted(PlayAudioMirrorIsCompletedSignal obj)
    {
        _audioSourceWhenMirrorIsCompleted.Play();
        _timer.OnTimerEnds += CheckAudioMirrorIsCompleted;
        _timer.SetTimeToWait(_audioSourceWhenMirrorIsCompleted.clip.length);
        Debug.Log($"DURACION AUDIO {_audioSourceWhenMirrorIsCompleted.clip.length} EMPEZAMOS en {Time.fixedTime}");
        StartCoroutine(_timer.TimerCoroutine());
    }

    private void CheckAudioMirrorIsCompleted()
    {
        Debug.Log($"TERMINAMOS  {Time.fixedTime}");

        _timer.OnTimerEnds -= CheckAudioMirrorIsCompleted;
        _endLevelModel.HasEndedAudioMirrorIsCompleted = true;
        if (_endLevelModel.HasEndedAudioMirrorIsCompleted && _endLevelModel.HasPressedKeyToGoThrowMirror)
        {
            Debug.Log("GO TO LEVEL 2");
            SceneManager.LoadScene("Nivel2");
        }
    }

    private void PlayAudioAfterTriggeredFridge(PlayAudioAfterPlayerTriggersFridgeSignal obj)
    {
        _audioSourceWhenPlayerTriggersFridge.Play();
    }

    private void PlayAudioVaseHitsGround(PlayVaseHitsGroundSignal obj)
    {
        _audioSourceWhenVaseHitsGround.Play();
    }


    private void PlayFirstAudioAfterPickingBedroomGlass(PlayAudioAfterPickingBedroomLevelOneGlassSignal obj)
    {
        _audioSourceWhenPlayerGetsGlassPieceBedroom.Play();
    }

    private void PlayAudioAfterPickingBathRoomGlass(PlayAudioAfterPickingBathRoomLevelOneGlassSignal obj)
    {
        _audioSourceWhenPlayerGetsGlassPieceBathroom.Play();
    }

    private void PlayHouseBackGroundMusicLevelOne(PlayHouseBackGroundMusicLevelOneSignal obj)
    {
        _audioSourceHouseBackgroundSound.Play();
    }

    private void PlayFirstAudioAfterTakingPicture(PlayAudioAfterTakingMotherPictureSignal obj)
    {
        _audioSourceStarterConversationWhenPlayerGetsPicture.Play();
    }

    private void StartConversationWhenPlayerHasPutFirstMirrorPiece(
        PlayConversationWhenPlayerHasPutFirstMirrorPieceSignal obj)
    {
        _audioSourceConversationWhenPlayerPutBathroomMirrorPiece.Play();
    }

    private void PlayWhenPlayerEntersBathRoom(PlayWhenPlayerEntersBathRoomSignal obj)
    {
        Debug.Log("SUena primer audio baño");
        _audioSourcePlayWhenPlayerEntersBathRoom.Play();
    }

    private void PlayBackGroundLevel(PlayBackgroundMusicLevelOneSignal signal)
    {
        Debug.Log("Suena musica de fondo");
        _audioSourceBackGround.Play();
    }

    private void PlayConversationAtStartLevel(DisableMovementAfterEventStartedSignal obj)
    {
        _firstAudioIsPlaying = true;
        Debug.Log("PLAY 1 AUDIO CONVERSACION");
        _audioSourceConversationAtStartLevel.Play();
        _timer.OnTimerEnds += EndStartConversationAtStartLevel;
        _timer.SetTimeToWait(_audioSourceConversationAtStartLevel.clip.length);
        StartCoroutine(_timer.TimerCoroutine());
    }

    private void EndStartConversationAtStartLevel()
    {
        Debug.Log("END AUDIO CONVERSACION");
        _timer.OnTimerEnds -= EndStartConversationAtStartLevel;
        _firstAudioIsPlaying = false;
        new EnablePlayerMovementSignal().Execute();
        new EnableMovementAfterEventEndedSignal().Execute();
    }
}