﻿using System;
using System.Collections;
using System.Collections.Generic;
using Presentation.Player;
using Signals.EventManager;
using UnityEngine;
using Utils;

public class LightManagerLevelOne : MonoBehaviour
{
    [SerializeField] private List<Light> _lights;
    [SerializeField] private float _timeToSwitchOnLights;
    private IEventManager _eventManager;
    private int _lightsEnded = 0;

    private void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    // Start is called before the first frame update
    void Start()
    {
        foreach (var light in _lights)
        {
            light.enabled = false;
        }

        _eventManager.AddActionToSignal<SwitchOffAllLightsLevelOneSignal>(SwitchOffAllLights);
        _eventManager.AddActionToSignal<SwitchOnAllLightsLevelOneSignal>(SwitchOnAllLights);
    }

    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<SwitchOffAllLightsLevelOneSignal>(SwitchOffAllLights);
        _eventManager.RemoveActionFromSignal<SwitchOnAllLightsLevelOneSignal>(SwitchOnAllLights);
    }

    private void SwitchOffAllLights(SwitchOffAllLightsLevelOneSignal obj)
    {
        Debug.Log("Apagamos luces");
        foreach (var light in _lights)
        {
            light.enabled = true;
            SwitcherLight switcherLight = new SwitcherLight(light, _timeToSwitchOnLights, true, 0);
            StartCoroutine(switcherLight.Start());
        }
    }

    private void SwitchOnAllLights(SwitchOnAllLightsLevelOneSignal obj)
    {
        foreach (var light in _lights)
        {
            light.enabled = true;
            SwitcherLight switcherLight = new SwitcherLight(light, _timeToSwitchOnLights, false, light.intensity);
            StartCoroutine(switcherLight.Start());
        }
    }
}