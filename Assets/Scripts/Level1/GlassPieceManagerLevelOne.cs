﻿using System.Linq;


public class GlassPieceManagerLevelOne : GlassPieceManager
{
    void Start()
    {
        base.Start();
        _eventManager.AddActionToSignal<EnableRestOfGlassPiecesSignal>(EnableRestOfPieces);
        _eventManager.AddActionToSignal<EnableGlassMirrorUnderTheBedSignal>(EnableGlassMirrorUnderTheBed);
        foreach (var glassPiece in _glassPieces)
        {
            if (glassPiece.IsEnabledAtFirst)
            {
                _idGlassEnabledAtFirst = glassPiece.GlassPiece.PieceId;
            }

            glassPiece.GlassPiece.gameObject.SetActive(glassPiece.IsEnabledAtFirst);
        }
    }


    protected override void PlayerGetsGlassPiece(PlayerGetsGlassPieceSignal obj)
    {
        base.PlayerGetsGlassPiece(null);

        var glassInitInfo = _glassPieces.Single(x => x.GlassPiece.PieceId == obj.PieceOfGlass);
        switch (glassInitInfo.Localization)
        {
            case LocalizationsLevelOne.BathRoom:
                new PlayAudioAfterPickingBathRoomLevelOneGlassSignal().Execute();
                break;
            case LocalizationsLevelOne.Bed:
                new PlayAudioAfterPickingBedroomLevelOneGlassSignal().Execute();
                break;
        }
    }

    private void EnableGlassMirrorUnderTheBed(EnableGlassMirrorUnderTheBedSignal obj)
    {
        GlassInitInfo glassInitInfo = _glassPieces.Single(x => x.Localization == LocalizationsLevelOne.Bed);
        glassInitInfo.GlassPiece.gameObject.SetActive(true);
    }

    protected override void PlayerHasPutGlassPieceOnMirror(PlayerPutsGlassPieceOnMirrorSignal obj)
    {
        base.PlayerHasPutGlassPieceOnMirror(null);

        if (_glassPiecesRecovered == _glassPieces.Count)
        {
            new PlayAudioMirrorIsCompletedSignal().Execute();
            new ShowFlashBackWhenMirrorIsCompletedLevelOneSignal().Execute();
            return;
        }

        GlassInitInfo glassInitInfo = _glassPieces.Single(x => x.GlassPiece.PieceId == obj.GlassPieceId);
        if (!glassInitInfo.IsEnabledAtFirst)
        {
            return;
        }

        PlayerHasPutBathGlassPieceInMirror();
    }

    private static void PlayerHasPutBathGlassPieceInMirror()
    {
        new PlayConversationWhenPlayerHasPutFirstMirrorPieceSignal().Execute();
        new EnableRestOfGlassPiecesSignal().Execute();
    }


    private void EnableRestOfPieces(EnableRestOfGlassPiecesSignal signal)
    {
        foreach (var glassPiece in _glassPieces)
        {
            if (glassPiece.GlassPiece.PieceId == _idGlassEnabledAtFirst)
            {
                continue;
            }

            if (glassPiece.Localization == LocalizationsLevelOne.Bed)
            {
                continue;
            }

            glassPiece.GlassPiece.gameObject.SetActive(true);
        }
    }
}