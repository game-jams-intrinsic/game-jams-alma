﻿using Presentation.Items;
using UnityEngine;
using Utils;

public class Furniture : InteractableItem
{
    [SerializeField] private Drawer _drawer;
    [SerializeField] private float _timeToMove, _distanceToMove;
    MoveCoroutine _moveCoroutine;

    private void Start()
    {
        _moveCoroutine = new MoveCoroutine(_drawer.transform, _drawer.transform.position,
            _drawer.transform.position + Vector3.forward * _distanceToMove, _timeToMove);
    }

    public override void InteractWithObject()
    {
        StartCoroutine(_moveCoroutine.Start());
    }

    public override void InteractWithObject(GameObject objectWhichInteract)
    {
    }
}