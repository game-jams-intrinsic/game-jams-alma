﻿using UnityEngine;

namespace Presentation.Items
{
    public abstract class InteractableBase : MonoBehaviour
    {
        private string _textToShow;
        
        public abstract void InteractWithObject();
        public abstract void InteractWithObject(GameObject objectWhichInteract);
        public abstract void ShowMessageOverItem();
        public abstract void HideMessageOverItem();


        protected void SetTextToShow(string textToShow)
        {
            _textToShow = textToShow;
        }

        protected void EnableTextToShow()
        {
            new ShowTextOnCanvasSignal() {TextToShow = _textToShow}.Execute();
        }

        protected void DisableTextToShow()
        {
            new HideTextOnCanvasSignal().Execute();
        }
    }
}