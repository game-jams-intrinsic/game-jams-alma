﻿using UnityEngine;

namespace Presentation.Items
{
    public abstract class InteractableItem : InteractableBase
    {
        public abstract override void InteractWithObject(GameObject objectWhichInteract);

        public override void ShowMessageOverItem()
        {
            EnableTextToShow();
        }

        public override void HideMessageOverItem()
        {
            DisableTextToShow();
        }
    }
}