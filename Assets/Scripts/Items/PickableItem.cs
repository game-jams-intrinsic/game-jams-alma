﻿using App;
using UnityEngine;

namespace Presentation.Items
{
    public abstract class PickableItem : InteractableBase
    {

        public abstract override void InteractWithObject();
        
        public override void ShowMessageOverItem()
        {
            EnableTextToShow();
        }

        public override void HideMessageOverItem()
        {
            DisableTextToShow();
        }
        
    }
}