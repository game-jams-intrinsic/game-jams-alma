﻿using System.Collections;
using UnityEngine;

namespace Utils
{
    public class RotateCoroutine : IEnumerator
    {
        public event System.Action<RotateCoroutine> OnCoroutineEnd = delegate { };

        public Transform ObjectTransform { get; }
        private Quaternion _originPosition, _destinationPosition;
        private float _movementSpeed;

        public RotateCoroutine(Transform objectTransform, Quaternion originPosition, Quaternion destinationPosition, float movementSpeed)
        {
            ObjectTransform = objectTransform;
            _originPosition = originPosition;
            _destinationPosition = destinationPosition;
            _movementSpeed = movementSpeed;
        }

        public IEnumerator Start()
        {
            float step = (_movementSpeed / (_originPosition.eulerAngles - _destinationPosition.eulerAngles).magnitude) * Time.fixedDeltaTime;
            float t = 0;
            while (t <= 1.0f)
            {
                t += step; // Goes from 0 to 1, incrementing by step each time
                ObjectTransform.rotation = Quaternion.Lerp(_originPosition, _destinationPosition, t); // Move objectToMove closer to b
                yield return new WaitForFixedUpdate();         // Leave the routine and return here in the next frame
            }
            ObjectTransform.rotation = _destinationPosition;
            OnCoroutineEnd(this);
        }

        public bool MoveNext()
        {
            return false;
            // throw new System.NotImplementedException();
        }

        public void Reset()
        {
            // throw new System.NotImplementedException();
        }

        public object Current { get; }
    }
}