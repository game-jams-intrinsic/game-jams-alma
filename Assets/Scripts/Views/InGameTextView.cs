﻿using System;
using Signals.EventManager;
using TMPro;
using UnityEngine;
using Utils;

public class InGameTextView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _text;
    private IEventManager _eventManager; 

    void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    private void Start()
    {
        _eventManager.AddActionToSignal<ShowTextOnCanvasSignal>(SetText);
        _eventManager.AddActionToSignal<HideTextOnCanvasSignal>(HideText);
        _text.enabled = false;
    }

    private void HideText(HideTextOnCanvasSignal obj)
    {
        _text.enabled = false;
        _text.SetText(String.Empty);
    }

    private void SetText(ShowTextOnCanvasSignal signal)
    {
        _text.enabled = true;
        _text.SetText(signal.TextToShow);
    }
}