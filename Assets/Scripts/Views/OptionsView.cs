﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OptionsView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _titleText, _languageText, _backButtonText;
    [SerializeField] private Button _backButton, _englishButton, _spanishButton;
    private ILanguageManager _languageManager;

    public event Action OnBackButtonIsPressed = delegate { };
    public event Action OnSpanishLanguageButtonIsPressed = delegate { };
    public event Action OnEnglishLanguageButtonIsPressed = delegate { };

    private void Start()
    {
        _backButton.onClick.AddListener(BackButton);
        _englishButton.onClick.AddListener(ChangeEnglishLanguage);
        _spanishButton.onClick.AddListener(ChangeSpanishLanguage);
    }


    private void OnDestroy()
    {
        _backButton.onClick.RemoveListener(BackButton);
        _englishButton.onClick.RemoveListener(ChangeEnglishLanguage);
        _spanishButton.onClick.RemoveListener(ChangeSpanishLanguage);
    }

    private void ChangeSpanishLanguage()
    {
        OnSpanishLanguageButtonIsPressed.Invoke();
    }

    private void ChangeEnglishLanguage()
    {
        OnEnglishLanguageButtonIsPressed.Invoke();
    }

    private void BackButton()
    {
        OnBackButtonIsPressed.Invoke();
    }

    public void SetLanguageText(ILanguageManager languageManager)
    {
        _languageManager = languageManager;
        _titleText.SetText(_languageManager.GetActualLanguageText().OptionsTitleTextInitSceneKey);
        _languageText.SetText(_languageManager.GetActualLanguageText().LanguageTitleTextInitSceneKey);
        _backButtonText.SetText(_languageManager.GetActualLanguageText().BackButtonTextInitSceneKey);
    }
}