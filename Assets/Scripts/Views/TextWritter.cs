﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Utils;

[Serializable]
public struct TextInfo
{
    public string TextToShowKey;
    public float DurationToFade;
    public float DurationToStay;
}

public class TextWritter : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _textMeshItem;
    private ILanguageManager _languageManager;

    [SerializeField] private List<TextInfo> _textToShow;
    private int _textIndex = 0;

    private FaderText _faderText;
    private Timer _timer;

    private bool _showingText;
    public event Action OnTextHasEnd = delegate { };

    public bool ShowingText => _showingText;

    private void Awake()
    {
        _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
    }

    void Start()
    {
        _showingText = true;
        _faderText = new FaderText(_textMeshItem);
        _timer = new Timer();
        _faderText.SetFadeType(true);
        SetValueOfText();
        _faderText.OnCoroutineEnd += WaitTimeBetweenTexts;
        StartCoroutine(_faderText.FadeIn());
    }

    public void TextHasEnd()
    {
        _showingText = false;
        StopCoroutine(_faderText);
        StopCoroutine(_timer);
        _textMeshItem.SetText("");
        OnTextHasEnd.Invoke();
    }

    private void SetValueOfText()
    {
        _textMeshItem.SetText(_languageManager.GetActualLanguageText()
            .GetStringTextScreenOfKey(_textToShow[_textIndex].TextToShowKey));
        _faderText.SetTimeToFade(_textToShow[_textIndex].DurationToFade);
    }

    private void WaitTimeBetweenTexts()
    {
        _faderText.OnCoroutineEnd -= WaitTimeBetweenTexts;
        _timer.SetTimeToWait(_textToShow[_textIndex].DurationToStay);
        _timer.OnTimerEnds += DoReverseFade;
        StartCoroutine(_timer.TimerCoroutine());
    }

    private void DoReverseFade()
    {
        _timer.OnTimerEnds -= DoReverseFade;
        _faderText.SetFadeType(false);
        _faderText.OnCoroutineEnd += ChangeText;

        StartCoroutine(_faderText.FadeIn());
    }

    private void ChangeText()
    {
        _faderText.OnCoroutineEnd -= ChangeText;
        if (_textIndex >= _textToShow.Count - 1)
        {
            TextHasEnd();
            return;
        }

        _textIndex++;
        SetValueOfText();
        _faderText.SetFadeType(true);
        _faderText.OnCoroutineEnd += WaitTimeBetweenTexts;

        StartCoroutine(_faderText.FadeIn());
    }
}