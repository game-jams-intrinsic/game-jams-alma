﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenuView : MonoBehaviour
{
    [SerializeField] private Button _continueButton, _exitButton;
    [SerializeField] private TextMeshProUGUI _continueButtonText, _exitButtonText, _titleMenu;
    public event Action OnContinueGame = delegate { };
    public event Action OnExitGame = delegate { };
    private ILanguageManager _languageManager;

    void Start()
    {
        _continueButton.onClick.AddListener(ContinueGame);
        _exitButton.onClick.AddListener(ExitGame);
    }

    public void SetLanguageTexts(ILanguageManager languageManager)
    {
        _languageManager = languageManager;
        _continueButtonText.SetText(_languageManager.GetActualLanguageText().ContinueButtonTextMenuPauseInGameSceneKey);
        _exitButtonText.SetText(_languageManager.GetActualLanguageText().ExitButtonTextMenuPauseInGameSceneKey);
        _titleMenu.SetText(_languageManager.GetActualLanguageText().TitleTextMenuPauseInGameSceneKey);
    }
    
    public void ContinueGame()
    {
        OnContinueGame.Invoke();
    }

    public void ExitGame()
    {
        OnExitGame.Invoke();
    }
    
}