﻿namespace Presentation.Player
{
    public class EndLevelModel : IEndLevelModel
    {
        public bool HasPressedKeyToGoThrowMirror { get; set; }
        public bool HasEndedAudioMirrorIsCompleted { get; set; }
    }
}