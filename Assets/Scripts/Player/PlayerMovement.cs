﻿using UnityEngine;
using Utils.Input;

public class PlayerMovement : MonoBehaviour
{
    private ReadInputPlayer _readInputPlayer;
    [SerializeField] private float _moveSpeed, _jumpSpeed, _distanceRaycast;
    [SerializeField] private LayerMask _layerMask;
    [SerializeField] private Rigidbody _rigidbody;

    private bool _isGround, _jumpPressed,_canMove;


    void Start()
    {
        Cursor.visible = false;
        _canMove = true;
    }
    public void Init(ReadInputPlayer readInputPlayer)
    {
        _readInputPlayer = readInputPlayer;
        _readInputPlayer.OnPlayerPressedSpaceKey += EnableJump;
    }

    private void EnableJump()
    {
        if (!_isGround)
        {
            return;
        }

        _jumpPressed = true;
    }

    void FixedUpdate()
    {
        if (!_canMove)
        {
            return;
        }
        
        Vector3 input = _readInputPlayer.MovementInGameAxis;
        Vector3 jumpMovement = Vector3.zero;
        _isGround = IsGround();

        if (_isGround && _jumpPressed)
        {
            Debug.Log($"JUMP{_jumpPressed}");
            jumpMovement = Vector3.up * _jumpSpeed;
            _jumpPressed = false;
            _rigidbody.AddForce(jumpMovement, ForceMode.Force);
        }

        if (input.magnitude < 0.01f)
        {
            return;
        }

        Vector3 movement = Camera.main.transform.right * input.x + Camera.main.transform.forward * input.z;
        _rigidbody.position += (movement) * Time.deltaTime * _moveSpeed;
    }

    private bool IsGround()
    {
        RaycastHit raycastHit;
        Physics.Raycast(transform.position, Vector3.down, out raycastHit, _distanceRaycast, _layerMask);
        return raycastHit.rigidbody;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, Vector3.down * _distanceRaycast);
    }

    public void EnableMovement()
    {
        _canMove = true;
        
    }

    public void DisableMovement()
    {
        _canMove = false;
    }
}