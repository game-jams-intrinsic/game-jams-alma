﻿using System;
using App.PlayerModelInfo;
using Cinemachine;
using Signals.EventManager;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;
using Utils.Input;

namespace Presentation.Player
{
    public class PlayerFacade : MonoBehaviour
    {
        [SerializeField] private ItemInteractor _itemInteractor;
        [SerializeField] private PlayerMovement _playerMovement;
        [SerializeField] private Camera _camera;
        [SerializeField] private CinemachineVirtualCamera _cameraVirtual;
        [SerializeField] private ActionToDo _actionToDoWhenPlayerPressToGoThrowMirror;

        private Vector3 positionToCheckByMouse;
        private Vector3 mousePosition;
        private IPlayerModel _playerModel;
        private IEndLevelModel _endLevelModel;
        private IEventManager _eventManager;
        private ReadInputPlayer _readInputPlayer;

        public IPlayerModel PlayerModel => _playerModel;

        void Awake()
        {
            _playerModel = ServiceLocator.Instance.GetModel<IPlayerModel>();
            _endLevelModel = ServiceLocator.Instance.GetModel<IEndLevelModel>();
            _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
            _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
            _playerMovement.Init(_readInputPlayer);
            _itemInteractor.Init(_camera, _playerModel);
            _eventManager.AddActionToSignal<PlayerGetsGlassPieceSignal>(AddPieceToGlassInventory);
            _eventManager.AddActionToSignal<EnablePlayerMovementSignal>(EnableInput);
            _eventManager.AddActionToSignal<DisablePlayerMovementSignal>(DisablePlayerMovementInput);
            _eventManager.AddActionToSignal<DisableMouseMovementSignal>(DisableMouseMovement);
            _eventManager.AddActionToSignal<ResetCameraRotationSignal>(ResetCameraRotation);
            _eventManager.AddActionToSignal<RemoveGlassPieceOfPlayerSignal>(RemovePieceOfGlass);
        }


        private void RemovePieceOfGlass(RemoveGlassPieceOfPlayerSignal obj)
        {
            _playerModel.CurrentGlassPiece = -1;
            _itemInteractor.ResetItemToPick();
        }

        private void ResetCameraRotation(ResetCameraRotationSignal obj)
        {
            transform.rotation = Quaternion.identity;
            _camera.transform.rotation = Quaternion.identity;
            _cameraVirtual.transform.rotation = Quaternion.identity;
        }

        private void Start()
        {
            _readInputPlayer.OnPlayerPressLeftButtonMouse += ClickLeftMouseButton;
            _readInputPlayer.OnPlayerPressGoThrowMirrorKey += PlayerPressGoThrowMirrorKey;
        }

        private void PlayerPressGoThrowMirrorKey()
        {
            _actionToDoWhenPlayerPressToGoThrowMirror.ExecuteAction();
            new ShowTextOnCanvasSignal() {TextToShow = String.Empty}.Execute();
            if (_endLevelModel.HasEndedAudioMirrorIsCompleted && _endLevelModel.HasPressedKeyToGoThrowMirror)
            {
                Debug.Log("GO TO LEVEL 2");
                SceneManager.LoadScene("Nivel2");
            }
        }

        private void AddPieceToGlassInventory(PlayerGetsGlassPieceSignal obj)
        {
            _playerModel.CurrentGlassPiece = obj.PieceOfGlass;
            Debug.Log($"Cogida pieza {obj.PieceOfGlass}");
        }

        private void OnDestroy()
        {
            _readInputPlayer.OnPlayerPressLeftButtonMouse -= ClickLeftMouseButton;
        }

        private void ClickLeftMouseButton(bool clicked)
        {
            if (!clicked)
            {
                return;
            }

            _itemInteractor.InteractWithItem();
        }

        private void DisablePlayerMovementInput(DisablePlayerMovementSignal obj)
        {
            _readInputPlayer.DisableGameplayInput();
        }

        private void EnableInput(EnablePlayerMovementSignal signal)
        {
            _readInputPlayer.EnableGameplayInput();
            _readInputPlayer.EnableMouseInput();
        }

        private void DisableMouseMovement(DisableMouseMovementSignal obj)
        {
            _readInputPlayer.DisableMouseInput();
        }
    }
}