﻿
namespace App.PlayerModelInfo
{
    public interface IPlayerModel
    {
        void ResetData();
        int CurrentGlassPiece { get; set; }
    }
}