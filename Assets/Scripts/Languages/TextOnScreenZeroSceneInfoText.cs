﻿using System;
using UnityEngine;

[Serializable]
public struct TextOnScreenZeroSceneInfoText
{
    public string Key;
    [TextArea] public string TextToShow;
}