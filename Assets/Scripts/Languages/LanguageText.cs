﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class LanguageText : ScriptableObject
{
    public string PlayButtonInitSceneKey;
    public string ExitButtonInitSceneKey;
    public string OptionsButtonInitSceneKey;
    public string ContinueButtonTextMenuPauseInGameSceneKey;
    public string ExitButtonTextMenuPauseInGameSceneKey;
    public string TitleTextMenuPauseInGameSceneKey;
    public string TextToOpenCloseDoorInGameKey;
    public string TextToPutPieceInMirrorInGameKey;
    public string TextToGetGlassPieceInGameKey;
    public string TextToGetTouchPianoInGameKey;
    public string TextToEscapeFromFlashbacksInGameKey;
    public string TextPressKeyToGoThrowGlassInGameKey;
    public string TextToGetPictureInGameKey;

    public string OptionsTitleTextInitSceneKey;
    public string LanguageTitleTextInitSceneKey;
    public string BackButtonTextInitSceneKey;
    public string CreditsTitleCreditsSceneKey;
    public string ProgrammersTitleCreditsSceneKey;
    public string ArtTitleCreditsSceneKey;
    public string DesignTitleCreditsSceneKey;
    public string TextToExitCreditsSceneKey;
    public string TextContinueCreditsSceneKey;
    public List<TextOnScreenZeroSceneInfoText> _textOnScreenZeroSceneText;

    public string GetStringTextScreenOfKey(string key)
    {
        return _textOnScreenZeroSceneText.Single(x => x.Key == key).TextToShow;
    }
}