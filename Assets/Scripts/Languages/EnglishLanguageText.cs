﻿using UnityEngine;

[CreateAssetMenu(fileName = "EnglishVersion", menuName = "Languages/English")]
public class EnglishLanguageText : LanguageText
{
}