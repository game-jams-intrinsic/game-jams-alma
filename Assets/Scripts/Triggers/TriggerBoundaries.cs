﻿using System;
using UnityEngine;

public class TriggerBoundaries : MonoBehaviour
{
    [SerializeField] private Transform _positionToResetPlayer;
    [SerializeField] private ColliderPassingChecker _colliderPassingChecker;

    private void Start()
    {
        _colliderPassingChecker.OnPlayerPassCollider += ResetPlayerPosition;
    }

    private void OnDestroy()
    {
        _colliderPassingChecker.OnPlayerPassCollider -= ResetPlayerPosition;
    }

    private void ResetPlayerPosition(Transform playerTransform)
    {
        playerTransform.position = _positionToResetPlayer.position;
    }
}