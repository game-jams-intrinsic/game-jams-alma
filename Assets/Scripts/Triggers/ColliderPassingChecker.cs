﻿using System;
using UnityEngine;


public enum EntranceAxis
{
    VERTICAL,
    HORIZONTAL
}


public abstract class ColliderPassingChecker : MonoBehaviour
{
    [SerializeField] protected BoxCollider _collider;
    [SerializeField] protected EntranceAxis _entranceAxis;
    protected Vector3 _sizeCollider;
    protected Vector3 _positionOfPlayerAtStartEntrance, _positionOfPlayerAtExitEntrance;
    public abstract bool HasBeenCrossedCompletelyByPlayer();
    public event Action<Transform> OnPlayerPassCollider = delegate { };

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Player"))
        {
            _positionOfPlayerAtStartEntrance = other.transform.position;
        }
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (other.transform.CompareTag("Player"))
        {
            _positionOfPlayerAtExitEntrance = other.transform.position;
    
            if (HasBeenCrossedCompletelyByPlayer())
            {
                OnPlayerPassCollider.Invoke(other.transform);
            }
        }
    }
}