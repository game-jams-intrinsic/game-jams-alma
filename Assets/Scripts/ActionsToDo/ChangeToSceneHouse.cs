﻿using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "ChangeToSceneHouse", menuName = "ActionsToDo/ChangeToSceneHouse")]
public class ChangeToSceneHouse : ActionToDo
{
    public override void ExecuteAction()
    {
        new MuteAllAudiosBeforeExitLevelZeroSignal().Execute();
        SceneManager.LoadScene("NewLvl1");
    }
}