﻿using UnityEngine;

[CreateAssetMenu(fileName = "ShowFlashbacksFridge", menuName = "ActionsToDo/ShowFlashbacksFridge")]
public class ShowFlashbacksFridge : ActionToDo
{
    public override void ExecuteAction()
    {
        Debug.Log("SHow FlashBackFridge");
        new ShowFlashbacksFridgeSignal().Execute();
        new PlayAudioAfterPlayerTriggersFridgeSignal().Execute();
    }
}