﻿using UnityEngine;

public abstract class ActionToDo : ScriptableObject
{
    public abstract void ExecuteAction();
}