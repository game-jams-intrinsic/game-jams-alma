﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utils;

public class CanvasInitPresenter : MonoBehaviour
{
    [SerializeField] private Button _buttonStart, _buttonExit, _buttonOptions;
    [SerializeField] private TextMeshProUGUI _buttonStartText, _buttonExitText, _buttonOptionsText;
    [SerializeField] private OptionsView _optionsView;
    [SerializeField] private Transform _initView;

    private ILanguageManager _languageManager;

    private void Awake()
    {
        _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
    }

    void Start()
    {
        HideOptions();
        _buttonStart.onClick.AddListener(StartGame);
        _buttonExit.onClick.AddListener(ExitGame);
        _buttonOptions.onClick.AddListener(ShowOptions);
        _optionsView.OnBackButtonIsPressed += HideOptions;
        _optionsView.OnEnglishLanguageButtonIsPressed += ChangeToEnglishLanguage;
        _optionsView.OnSpanishLanguageButtonIsPressed += ChangeToSpanishLanguage;
        SetLanguageTexts();
    }

    private void ChangeToSpanishLanguage()
    {
        _languageManager.SetActualLanguageText(LanguagesKeys.SPA);
        SetLanguageTexts();

    }

    private void ChangeToEnglishLanguage()
    {
        _languageManager.SetActualLanguageText(LanguagesKeys.ENG);
        SetLanguageTexts();

    }

    private void HideOptions()
    {
        _initView.gameObject.SetActive(true);
        _optionsView.gameObject.SetActive(false);
    }

    private void ShowOptions()
    {
        _initView.gameObject.SetActive(false);
        _optionsView.gameObject.SetActive(true);
        _optionsView.SetLanguageText(_languageManager);
    }

    private void SetLanguageTexts()
    {
        _buttonStartText.SetText(_languageManager.GetActualLanguageText().PlayButtonInitSceneKey);
        _buttonExitText.SetText(_languageManager.GetActualLanguageText().ExitButtonInitSceneKey);
        _buttonOptionsText.SetText(_languageManager.GetActualLanguageText().OptionsButtonInitSceneKey);
        _optionsView.SetLanguageText(_languageManager);

    }

    private void OnDestroy()
    {
        _buttonStart.onClick.RemoveListener(StartGame);
        _buttonExit.onClick.RemoveListener(ExitGame);
    }

    private void ExitGame()
    {
        Application.Quit();
    }

    private void StartGame()
    {
        SceneManager.LoadScene("Level0");
    }
}