﻿using Signals.EventManager;
using UnityEngine;
using Utils;

public class CameraCullingMaskManager : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    private IEventManager _eventManager;
    private LayerMask _originalLayerMask;
    [SerializeField] private LayerMask _layerMaskToShow;

    private void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    private void Start()
    {
        _eventManager.AddActionToSignal<ShowOnlyUIMaskSignal>(ShowOnlyUIMaskForFlashbackOrImages);

        _eventManager.AddActionToSignal<ShowAllLayersInCullingMaskSignal>(ShowAllMask);
        _originalLayerMask = _camera.cullingMask;
    }

    private void ShowAllMask(ShowAllLayersInCullingMaskSignal obj)
    {
        _camera.cullingMask = _originalLayerMask;
    }

    private void ShowUILayerOnCamera()
    {
        _camera.cullingMask = _layerMaskToShow;
    }

    private void ShowOnlyUIMaskForFlashbackOrImages(ShowOnlyUIMaskSignal obj)
    {
        ShowUILayerOnCamera();
    }
}