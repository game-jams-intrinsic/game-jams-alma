﻿using System;
using UnityEngine;

[Serializable]
public struct MirrorPieces
{
    public int IdPiece;
    public Transform Piece;
}