﻿using UnityEngine;
using Utils;

[CreateAssetMenu(fileName = "ShowTextToMakePlayerPressInput", menuName = "ActionsToDo/ShowTextToMakePlayerPressInput")]
public class ShowTextToMakePlayerPressInput : ActionToDo
{
    public override void ExecuteAction()
    {
        new ShowTextOnCanvasSignal()
        {
            TextToShow = ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageText()
                .TextPressKeyToGoThrowGlassInGameKey
        }.Execute();
    }
}