﻿using Cinemachine;
using Signals.EventManager;
using UnityEngine;
using Utils;

public class CameraShakeControl : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera _cinemachineVirtualCamera;
    [SerializeField] private float _amplitudeGain, _frecuenciaGain;

    private CinemachineBasicMultiChannelPerlin _virtualCameraNoise;
    private IEventManager _eventManager;
    private Timer _timer;
    private bool _shake = false;
    private ActionToDo _actionToDoAfterShake;
    private void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    void Start()
    {
        _actionToDoAfterShake = null;
        _timer = new Timer();
        _virtualCameraNoise = _cinemachineVirtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        _eventManager.AddActionToSignal<ShakeCameraSignal>(ShakeCameraOverTime);
    }

    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<ShakeCameraSignal>(ShakeCameraOverTime);
    }

    private void ShakeCameraOverTime(ShakeCameraSignal obj)
    {
        _shake = true;
        _timer.SetTimeToWait(obj.TimeToShake);
        _timer.OnTimerEnds += DisableShake;
        StartCoroutine(_timer.TimerCoroutine());
        _actionToDoAfterShake = obj.ActionToDoAfter;
    }

    private void DisableShake()
    {
        Debug.Log("END SHAKE");
        _timer.OnTimerEnds -= DisableShake;
        _virtualCameraNoise.m_AmplitudeGain = 0;
        _virtualCameraNoise.m_FrequencyGain = 0;
        _shake = false;
        _actionToDoAfterShake.ExecuteAction();
    }

    void Update()
    {
        if (!_virtualCameraNoise || !_shake)
        {
            return;
        }

        _virtualCameraNoise.m_AmplitudeGain = _amplitudeGain;
        _virtualCameraNoise.m_FrequencyGain = _frecuenciaGain;
    }
}

