﻿using Signals.EventManager;
using UnityEngine;
using Utils;
using Utils.Input;

public class CameraRotator : MonoBehaviour
{
    private ReadInputPlayer _readInputPlayer;

    [SerializeField] private PlayerMovement _playerMovement;
    [SerializeField] private float _mouseSensivity, _minRotationY, _maxRotationY;

    private float _xMouseValue, _yMouseValue;
    private float _xRotation, _yRotation;
    private bool _isRotating, _canRotate;
    private Vector2 _mouseInput;
    private IEventManager _eventManager;

    void Awake()
    {
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    private void Start()
    {
        _readInputPlayer.OnPlayerMovesMouse += GetRotationsValuesOfMouse;
        _readInputPlayer.OnPlayerStopMovesMouse += StopRotationsValuesOfMouse;
        _canRotate = true;
        _eventManager.AddActionToSignal<StopRotationCameraSignal>(StopRotationsValuesOfMouseBySignal);
        _eventManager.AddActionToSignal<ResumeRotationCameraSignal>(ResumeRotationCameraBySignal);
        _isRotating = false;
        Cursor.lockState = CursorLockMode.Locked;
        // Cursor.visible = false;
    }

    private void OnDestroy()
    {
        _readInputPlayer.OnPlayerMovesMouse -= GetRotationsValuesOfMouse;
        _eventManager.RemoveActionFromSignal<StopRotationCameraSignal>(StopRotationsValuesOfMouseBySignal);
        _eventManager.RemoveActionFromSignal<ResumeRotationCameraSignal>(ResumeRotationCameraBySignal);
    }

    private void StopRotationsValuesOfMouseBySignal(StopRotationCameraSignal obj)
    {
        _canRotate = false;
    }

    private void ResumeRotationCameraBySignal(ResumeRotationCameraSignal obj)
    {
        _canRotate = true;
    }

    private void StopRotationsValuesOfMouse(Vector2 obj)
    {
        _mouseInput = Vector2.zero;
        _isRotating = false;
    }

    private void GetRotationsValuesOfMouse(Vector2 obj)
    {
        _isRotating = true;

        _mouseInput = obj;
    }

    void Update()
    {
        if (!_canRotate || !_isRotating)
        {
            return;
        }
        GetRotationValuesCamera();
        RotateCamera();
    }

    private void GetRotationValuesCamera()
    {
        float xMouseValue = _mouseInput.y * Time.deltaTime * _mouseSensivity;
        float yMouseValue = _mouseInput.x * Time.deltaTime * _mouseSensivity;

        _xRotation -= xMouseValue;
        _yRotation -= yMouseValue;

        _xRotation = Mathf.Clamp(_xRotation, _minRotationY, _maxRotationY);
    }

    private void RotateCamera()
    {
        transform.localEulerAngles = new Vector3(_xRotation, -_yRotation, 0.0f);
        _playerMovement.transform.Rotate(Vector3.up * _xMouseValue);
    }
}