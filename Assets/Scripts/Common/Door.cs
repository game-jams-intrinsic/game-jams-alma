﻿using Presentation.Items;
using UnityEngine;
using Utils;

public class Door : InteractableItem
{
    [SerializeField] private ColliderPassingChecker _colliderPassingChecker;
    [SerializeField] private ActionToDo _actionToDo;
    [SerializeField] private Animator _animator;
    [SerializeField] private bool _isOpen = false;
    private static readonly int Close = Animator.StringToHash("Close");
    private static readonly int Open = Animator.StringToHash("Open");

    private void Start()
    {
        SetTextToShow(ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageText().TextToOpenCloseDoorInGameKey);
        _colliderPassingChecker.OnPlayerPassCollider += ExecuteAction;
        if (_isOpen)
        {
            OpenDoor();
        }
    }

    private void OpenDoor()
    {
        _animator.SetBool(Open, true);
        _animator.SetBool(Close, false);
    }

    private void ExecuteAction(Transform obj)
    {
        if (!_actionToDo)
        {
            return;
        }
        _actionToDo.ExecuteAction();
    }

    public override void InteractWithObject()
    {
        if (_isOpen)
        {
            CloseDoor();
        }
        else
        {
            OpenDoor();
        }

        _isOpen = !_isOpen;
        Debug.Log("Interactuamos con Puerta");
    }

    private void CloseDoor()
    {
        _animator.SetBool(Open, false);
        _animator.SetBool(Close, true);
    }

    public override void InteractWithObject(GameObject objectWhichInteract)
    {
        InteractWithObject();
    }
}