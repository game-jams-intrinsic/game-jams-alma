﻿using System.Linq;
using App.PlayerModelInfo;
using Signals.EventManager;
using UnityEngine;
using UnityEngine.UI;
using Utils;
using Utils.Input;

public abstract class CanvasLevelPresenter : MonoBehaviour
{
    [SerializeField] protected Image _pieceImage;
    [SerializeField] private GlassPieceManager _glassPieceManager;
    [SerializeField] protected FlashBackViewLevel _flashBackViewLevel;
    private bool _canHideImages;

    protected IEventManager _eventManager;
    private ReadInputPlayer _readInputPlayer;
    private IPlayerModel _playerModel;
    private ILanguageManager _languageManager;

    protected void Awake()
    {
        _pieceImage.gameObject.SetActive(false);

        _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
        _playerModel = ServiceLocator.Instance.GetModel<IPlayerModel>();
    }

    protected void Start()
    {
        _flashBackViewLevel.OnRemoveFlashbacks += RemoveFlashback;
        _flashBackViewLevel.gameObject.SetActive(false);
        _flashBackViewLevel.HideInfoText();
        _readInputPlayer.OnDebugEscPressed += HideImages;
        
        _eventManager.AddActionToSignal<PlayerGetsGlassPieceSignal>(ShowPiece);
        _eventManager.AddActionToSignal<HideGlassPieceOfCanvasSignal>(PlayerPutsGlassPieceOnMirror);
        _eventManager.AddActionToSignal<ShowAllLayersInCullingMaskSignal>(ShowGlassPieceAgain);
        _eventManager.AddActionToSignal<ShowPictureOnCanvasSignal>(ShowPicture);
    }
    protected void OnDestroy()
    {
        _flashBackViewLevel.OnRemoveFlashbacks -= RemoveFlashback;
        _eventManager.RemoveActionFromSignal<PlayerGetsGlassPieceSignal>(ShowPiece);
        _eventManager.RemoveActionFromSignal<HideGlassPieceOfCanvasSignal>(PlayerPutsGlassPieceOnMirror);
        _eventManager.RemoveActionFromSignal<ShowPictureOnCanvasSignal>(ShowPicture);
        _readInputPlayer.OnDebugEscPressed -= HideImages;
    }

    private void ShowGlassPieceAgain(ShowAllLayersInCullingMaskSignal obj)
    {
        if (_playerModel.CurrentGlassPiece > 0)
        {
            _pieceImage.gameObject.SetActive(true);
        }
    }


    private void PlayerPutsGlassPieceOnMirror(HideGlassPieceOfCanvasSignal obj)
    {
        HideGlassPiece();
        _pieceImage.sprite = null;
    }

    protected void HideGlassPiece()
    {
        _pieceImage.gameObject.SetActive(false);
    }

    private void ShowPiece(PlayerGetsGlassPieceSignal obj)
    {
        _pieceImage.gameObject.SetActive(true);
        var piece = _glassPieceManager.GlassPieces.Single(x => x.GlassPiece.PieceId == obj.PieceOfGlass);
        _pieceImage.sprite = piece.SpriteToShow;
    }

    private void RemoveFlashback()
    {
        ShowGlassPieceAgain(null);
        _flashBackViewLevel.gameObject.SetActive(false);
        new ShowAllLayersInCullingMaskSignal().Execute();
        new EnablePlayerMovementSignal().Execute();
        new IsNotShowingFlashBackOrImagesSignal().Execute();
    }
    
    private void ShowPicture(ShowPictureOnCanvasSignal obj)
    {
        new ShowFlashBackOrImagesSignal().Execute();

        new ShowOnlyUIMaskSignal().Execute();
        _canHideImages = true;
        _pieceImage.gameObject.SetActive(false);
        _flashBackViewLevel.gameObject.SetActive(true);
        _flashBackViewLevel.ShowPicture(obj.PictureToShow);
        _flashBackViewLevel.ShowInfoText(_languageManager.GetActualLanguageText().TextToEscapeFromFlashbacksInGameKey);
        new DisableMouseMovementSignal().Execute();
        new DisablePlayerMovementSignal().Execute();
    }
    
    
    private void HideImages()
    {
        if (!_flashBackViewLevel.isActiveAndEnabled || !_canHideImages)
        {
            return;
        }

        _canHideImages = false;
        _flashBackViewLevel.HidePicture();
        _flashBackViewLevel.HideInfoText();
        ShowGlassPieceAgain(null);

        _flashBackViewLevel.gameObject.SetActive(false);
        new HidePictureOnCanvasSignal().Execute();
        new EnablePlayerMovementSignal().Execute();
        new ShowAllLayersInCullingMaskSignal().Execute();
        new IsNotShowingFlashBackOrImagesSignal().Execute();
    }
}