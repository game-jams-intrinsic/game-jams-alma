﻿using Presentation.Items;
using UnityEngine;
using Utils;

public class Picture : InteractableItem
{
    [SerializeField] private Sprite _sprite;
    [SerializeField] private ActionToDo _actionToDo;
    private bool _pictureHasBeenPickedBefore;
    private void Start()
    {
        _pictureHasBeenPickedBefore = false;
        SetTextToShow(ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageText().TextToGetPictureInGameKey);
    }

    public override void InteractWithObject()
    {
        new ShowPictureOnCanvasSignal() {PictureToShow = _sprite}.Execute();
        if (!_actionToDo || _pictureHasBeenPickedBefore)
        {
            return;
        }
        _pictureHasBeenPickedBefore = true;

        _actionToDo.ExecuteAction();

    }

    public override void InteractWithObject(GameObject objectWhichInteract)
    {
        InteractWithObject();
    }
}