﻿using UnityEngine;

public class TriggerWhenPlayerGoToDoor : MonoBehaviour
{
    private bool _hasTriggered;
    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("Player") || _hasTriggered)
        {
            return;
        }

        _hasTriggered = true;
        new PlayWhenTriggerLockedDoorSoundSignal().Execute();
    }
}