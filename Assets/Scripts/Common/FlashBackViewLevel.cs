﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public abstract class FlashBackViewLevel : MonoBehaviour
{
    [SerializeField] protected GameObject _background, _foregound;
    [SerializeField] protected TextMeshProUGUI _infoText;
    [SerializeField] protected Image _spriteRenderer;
    [SerializeField] protected float _timeToShowSprites;
    [SerializeField] protected List<Sprite> _flashbacksToShowAfterCompleteMirror;
    [SerializeField] protected ActionToDo _actionToDoAfterCameraShakeWhenPlayerCompletesMirror;
    public event Action OnRemoveFlashbacks = delegate { };
    protected Timer _timer;

    
    public void RemoveFlashBack()
    {
        _timer.OnTimerEnds -= RemoveFlashBack;
        OnRemoveFlashbacks.Invoke();
    }

    public void ShowPicture(Sprite objPictureToShow)
    {
        _background.gameObject.SetActive(true);
        _foregound.gameObject.SetActive(true);
        _spriteRenderer.gameObject.SetActive(true);
        _spriteRenderer.sprite = objPictureToShow;
    }

    public void HidePicture()
    {
        _background.gameObject.SetActive(false);
        _foregound.gameObject.SetActive(false);
        _spriteRenderer.gameObject.SetActive(false);
    }

    public void HideInfoText()
    {
        _infoText.SetText(String.Empty);
        _infoText.gameObject.SetActive(false);
    }

    public void ShowInfoText(string textToShow)
    {
        _infoText.gameObject.SetActive(true);
        _infoText.SetText(textToShow);
    }
    
    protected void EnableStructureToShowFlashbacks()
    {
        _background.gameObject.SetActive(true);
        _foregound.gameObject.SetActive(true);
        _spriteRenderer.gameObject.SetActive(true);
    }
    private void Awake()
    {
        _background.gameObject.SetActive(false);
        _foregound.gameObject.SetActive(false);
        _spriteRenderer.gameObject.SetActive(false);
        _timer = new Timer();
    }
    
}