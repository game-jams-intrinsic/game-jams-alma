﻿using Cinemachine;
using Signals.EventManager;
using UnityEngine;
using Utils;

public class CameraRotatorAutomatization : MonoBehaviour
{
    private RotateCoroutine _moveCoroutine;
    private IEventManager _eventManager;
    [SerializeField] private CinemachineVirtualCamera _cameraToRotate;
    [SerializeField] private float _angleToMoveToRight,
        _angleToMoveToLeft,
        _speedToMoveToLeft,
        _speedToMoveToRight,
        _speedToMoveToCenter;

    void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    void Start()
    {
        _eventManager.AddActionToSignal<StartMovementCameraAfterEnterCenterZoneSignal>(StartMovementToShowDoors);
    }

    private void StartMovementToShowDoors(StartMovementCameraAfterEnterCenterZoneSignal obj)
    {
        MoveCameraToLeftDoor();
    }

    private void MoveCameraToLeftDoor()
    {
        _moveCoroutine = new RotateCoroutine(_cameraToRotate.transform, _cameraToRotate.transform.rotation,
            Quaternion.AngleAxis(_angleToMoveToLeft, Vector3.up), _speedToMoveToLeft);
        _moveCoroutine.OnCoroutineEnd += MoveCameraToCenter;
        
        new EnableLightLeftDoorThirdPartSignal().Execute();
        StartCoroutine(_moveCoroutine.Start());
    }

    private void MoveCameraToCenter(RotateCoroutine obj)
    {
        _moveCoroutine.OnCoroutineEnd -= MoveCameraToCenter;

        _moveCoroutine = new RotateCoroutine(_cameraToRotate.transform, _cameraToRotate.transform.rotation,
            Quaternion.identity, _speedToMoveToCenter);
        _moveCoroutine.OnCoroutineEnd += MoveCameraToRightDoor;
        
        new EnableLightCenterDoorThirdPartSignal().Execute();
        StartCoroutine(_moveCoroutine.Start());
    }

    private void MoveCameraToRightDoor(RotateCoroutine obj)
    {
        _moveCoroutine.OnCoroutineEnd -= MoveCameraToRightDoor;
        _moveCoroutine = new RotateCoroutine(_cameraToRotate.transform, _cameraToRotate.transform.rotation,
            Quaternion.AngleAxis(_angleToMoveToRight, Vector3.up), _speedToMoveToRight);
        _moveCoroutine.OnCoroutineEnd += EndCameraMovement;
        
        new EnableLightRightDoorThirdPartSignal().Execute();
        StartCoroutine(_moveCoroutine.Start());
    }

    private void EndCameraMovement(RotateCoroutine obj)
    {
        Debug.Log("END CAMERA MOVEMENT");
        _moveCoroutine.OnCoroutineEnd -= EndCameraMovement;

        new EnablePlayerMovementSignal().Execute();
    }
}