﻿using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private Transform _playerTransform;
    [SerializeField] private float _cameraHight;

    void Start()
    {
        transform.position = _playerTransform.position;
    }

    void Update()
    {
        transform.position = new Vector3(_playerTransform.position.x, _playerTransform.position.y+_cameraHight, _playerTransform.position.z);
    }
}