﻿using System;
using UnityEngine;


public class GlassInitInfo
{
    public GlassPiece GlassPiece;
    public bool IsEnabledAtFirst;
    public Sprite SpriteToShow;
}