﻿using Presentation.Player;
using Signals.EventManager;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;

public class AudioManagerLevelTwo : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSourceBackGround;
    [SerializeField] private AudioSource _audioSourceStreetLevelBackGround;
    [SerializeField] private AudioSource _audioSourceWhenPlayerGoToConservatorySound;
    [SerializeField] private AudioSource _audioSourceWhenPlayerGoToChildrenParkAudioFirst;
    [SerializeField] private AudioSource _audioSourceWhenPlayerGoToChildrenParkAudioSecond;
    [SerializeField] private AudioSource _audioSourceWhenPlayerGoToPianoForFirstTimeSound;
    [SerializeField] private AudioSource _audioSourceInitialAudioWhenLevelStartsAudio;
    [SerializeField] private AudioSource _audioSourceWhenPlayerIsInFrontOfMirrorForFirstTimeAudio;
    [SerializeField] private AudioSource _audioSourceWhenPlayerPutGlassPieceInMirror;
    [SerializeField] private AudioSource _audioSourceWhenPlayerGoThrowFenceConversatory;
    [SerializeField] private AudioSource _audioSourceWhenPlayerCompletesMirror;
    [SerializeField] private AudioSource _audioSourcePlayWhenPlayerGoToEntranceInside;
    [SerializeField] private AudioSource _audioSourceWhenPlayerTakesDaughtersPicture;
    [SerializeField] private AudioSource _audioSourceWhenPlayerTouchesPiano;

    private IEventManager _eventManager;
    private Timer _timer;
    private IEndLevelModel _endLevelModel;

    private bool _firstAudioIsPlaying;

    private void Awake()
    {
        _audioSourceBackGround.Play();
        _audioSourceStreetLevelBackGround.Play();
        _audioSourceInitialAudioWhenLevelStartsAudio.Play();


        _timer = new Timer();
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
        _endLevelModel = ServiceLocator.Instance.GetModel<IEndLevelModel>();

        _eventManager.AddActionToSignal<PlayChildrenParkConversationSignal>(PlayFirstChildrenParkConversation);
        _eventManager.AddActionToSignal<PlayConservatoryConversationSignal>(PlayConservatoryConversation);
        _eventManager.AddActionToSignal<PlayWhenPlayerGoToPianoConversationSignal>(PlayWhenPlayerGoToPianoConversation);
        _eventManager.AddActionToSignal<PlayWhenPlayerIsInFrontOfMirrorLevelTwoSignal>(PlayWhenPlayerIsInFrontOfMirror);
        _eventManager.AddActionToSignal<PlayGlassPiecesIsPutInMirrorAudioSignal>(PlayGlassPiecesIsPutInMirrorAudio);
        _eventManager.AddActionToSignal<PlayWhenPlayerGoThrowFenceConservatorySignal>(
            PlayWhenPlayerGoThrowFenceConservatory);
        _eventManager.AddActionToSignal<PlayWhenPlayerGoToEntranceInsideSignal>(
            PlayWhenPlayerGoToEntranceInside);
        _eventManager.AddActionToSignal<PlayAudioAfterTakingDaughtersPictureSignal>(
            PlayAudioAfterTakingDaughtersPicture);
        _eventManager.AddActionToSignal<PlayAudioMirrorIsCompletedSignal>(PlayAudioMirrorIsCompleted);
        _eventManager.AddActionToSignal<PlayAudioWhenPlayerTouchesPianoSignal>(PlayAudioWhenPlayerTouchesPiano);
    }

    private void PlayAudioWhenPlayerTouchesPiano(PlayAudioWhenPlayerTouchesPianoSignal obj)
    {
        throw new System.NotImplementedException();
    }

    private void PlayAudioAfterTakingDaughtersPicture(PlayAudioAfterTakingDaughtersPictureSignal obj)
    {
        _audioSourceWhenPlayerTakesDaughtersPicture.Play();
    }

    private void PlayWhenPlayerGoToEntranceInside(PlayWhenPlayerGoToEntranceInsideSignal obj)
    {
        _audioSourcePlayWhenPlayerGoToEntranceInside.Play();
    }

    private void PlayWhenPlayerGoThrowFenceConservatory(PlayWhenPlayerGoThrowFenceConservatorySignal obj)
    {
        _audioSourceWhenPlayerGoThrowFenceConversatory.Play();
    }

    private void PlayGlassPiecesIsPutInMirrorAudio(PlayGlassPiecesIsPutInMirrorAudioSignal obj)
    {
        _audioSourceWhenPlayerPutGlassPieceInMirror.Play();
    }


    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<PlayChildrenParkConversationSignal>(PlayFirstChildrenParkConversation);
        _eventManager.RemoveActionFromSignal<PlayConservatoryConversationSignal>(PlayConservatoryConversation);
        _eventManager.RemoveActionFromSignal<PlayWhenPlayerGoToPianoConversationSignal>(
            PlayWhenPlayerGoToPianoConversation);
        _eventManager.RemoveActionFromSignal<PlayWhenPlayerIsInFrontOfMirrorLevelTwoSignal>(
            PlayWhenPlayerIsInFrontOfMirror);

        _eventManager.RemoveActionFromSignal<PlayAudioMirrorIsCompletedSignal>(PlayAudioMirrorIsCompleted);
    }

    private void PlayWhenPlayerIsInFrontOfMirror(PlayWhenPlayerIsInFrontOfMirrorLevelTwoSignal obj)
    {
        _audioSourceWhenPlayerIsInFrontOfMirrorForFirstTimeAudio.Play();
    }


    private void PlayFirstChildrenParkConversation(PlayChildrenParkConversationSignal obj)
    {
        _audioSourceWhenPlayerGoToChildrenParkAudioFirst.Play();
        _timer.OnTimerEnds += PlaySecondChildrenParkConversation;
        _timer.SetTimeToWait(_audioSourceWhenPlayerGoToChildrenParkAudioFirst.clip.length);
        StartCoroutine(_timer.TimerCoroutine());
    }

    private void PlaySecondChildrenParkConversation()
    {
        _timer.OnTimerEnds -= PlaySecondChildrenParkConversation;
        _audioSourceWhenPlayerGoToChildrenParkAudioSecond.Play();
    }


    private void PlayWhenPlayerGoToPianoConversation(PlayWhenPlayerGoToPianoConversationSignal obj)
    {
        _audioSourceWhenPlayerGoToPianoForFirstTimeSound.Play();
    }

    private void PlayConservatoryConversation(PlayConservatoryConversationSignal obj)
    {
        _audioSourceWhenPlayerGoToConservatorySound.Play();
    }

    private void PlayAudioMirrorIsCompleted(PlayAudioMirrorIsCompletedSignal obj)
    {
        _audioSourceWhenPlayerCompletesMirror.Play();
        _timer.OnTimerEnds += CheckAudioMirrorIsCompleted;
        _timer.SetTimeToWait(_audioSourceWhenPlayerCompletesMirror.clip.length);
        StartCoroutine(_timer.TimerCoroutine());
    }

    private void CheckAudioMirrorIsCompleted()
    {
        _timer.OnTimerEnds -= CheckAudioMirrorIsCompleted;
        _endLevelModel.HasEndedAudioMirrorIsCompleted = true;
        if (_endLevelModel.HasEndedAudioMirrorIsCompleted && _endLevelModel.HasPressedKeyToGoThrowMirror)
        {
            SceneManager.LoadScene("Level4");
        }
    }
}