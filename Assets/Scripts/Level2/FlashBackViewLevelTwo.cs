﻿public class FlashBackViewLevelTwo : FlashBackViewLevel
{
    private int _flashBackAfterCompleteMirrorIndex = 0;

    private void Awake()
    {
        _background.gameObject.SetActive(false);
        _foregound.gameObject.SetActive(false);
        _spriteRenderer.gameObject.SetActive(false);
        _timer = new Timer();
    }

    public void ShowFlashWhenMirrorIsCompleted()
    {
        EnableStructureToShowFlashbacks();
        ShowSpritesOfListSpritesMirrorIsCompleted();
    }

    private void ShowSpritesOfListSpritesMirrorIsCompleted()
    {
        _timer.OnTimerEnds -= ShowSpritesOfListSpritesMirrorIsCompleted;
        if (_flashBackAfterCompleteMirrorIndex >= _flashbacksToShowAfterCompleteMirror.Count)
        {
            _timer.OnTimerEnds -= ShowSpritesOfListSpritesMirrorIsCompleted;
            RemoveFlashBack();
            return;
        }

        _spriteRenderer.sprite = _flashbacksToShowAfterCompleteMirror[_flashBackAfterCompleteMirrorIndex];
        _timer.SetTimeToWait(_timeToShowSprites);
        _timer.OnTimerEnds += ShowSpritesOfListSpritesMirrorIsCompleted;
        StartCoroutine(_timer.TimerCoroutine());
        _flashBackAfterCompleteMirrorIndex++;
    }
}