﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitialActionsLevelTwo : MonoBehaviour
{
    [SerializeField] private Mirror _mirrorToRemoveAfterStart;
    [SerializeField] private float _timeToRemoveMirrorAfterStart;

    private Timer _timer;

    // Start is called before the first frame update
    void Start()
    {
        _timer = new Timer();
        StartTimerToDeleteMirrorAfterStart();
    }

    private void StartTimerToDeleteMirrorAfterStart()
    {
        _timer.SetTimeToWait(_timeToRemoveMirrorAfterStart);
        _timer.OnTimerEnds += RemoveMirror;
        StartCoroutine(_timer.TimerCoroutine());
    }


    private void RemoveMirror()
    {
        _timer.OnTimerEnds -= RemoveMirror;

        _mirrorToRemoveAfterStart.gameObject.SetActive(false);
    }


}