﻿using System;

[Serializable]
public class GlassInitInfoLevelTwo : GlassInitInfo
{
    public LocalizationsLevelTwo Localization;
}