﻿using System.Collections.Generic;
using System.Linq;
using Signals.EventManager;
using UnityEngine;
using Utils;


public class GlassPieceManagerLevelTwo : GlassPieceManager
{
    void Start()
    {
        _glassPiecesRecovered = 0;
        _eventManager.AddActionToSignal<PlayerPutsGlassPieceOnMirrorSignal>(PlayerHasPutGlassPieceOnMirror);
        _eventManager.AddActionToSignal<PlayerGetsGlassPieceSignal>(PlayerGetsGlassPiece);
    }


    protected override void PlayerGetsGlassPiece(PlayerGetsGlassPieceSignal obj)
    {
        base.PlayerGetsGlassPiece(null);

    }


    protected override void PlayerHasPutGlassPieceOnMirror(PlayerPutsGlassPieceOnMirrorSignal obj)
    {

        base.PlayerHasPutGlassPieceOnMirror(null);
        // GlassInitInfoLevelTwo glassInitInfo = _glassPieces.Single(x => x.GlassPiece.PieceId == obj.GlassPieceId);
        // switch (glassInitInfo.Localization)
        // {
        //     case LocalizationsLevelTwo.Balcony:
        //         new PlayAudioAfterPickingBalconyLevelTwoGlassSignal().Execute();
        //         break;
        //     case LocalizationsLevelTwo.Conservatory:
        //         break;
        //     case LocalizationsLevelTwo.Container:
        //         new PlayAudioAfterPickingContainerLevelTwoGlassSignal().Execute();
        //         break;
        //     case LocalizationsLevelTwo.ChildrenPark:
        //         new PlayAudioAfterPuttingChildrenParkInMirrorLevelTwoGlassSignal().Execute();
        //         break;
        // }
        if (_glassPiecesRecovered != _glassPieces.Count) return;
        new PlayAudioMirrorIsCompletedSignal().Execute();
        new SwitchOffAllLightsLevelTwoSignal().Execute();
        new ShowFlashBackWhenMirrorIsCompletedLevelTwoSignal().Execute();
    }
}