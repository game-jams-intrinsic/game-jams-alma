﻿using System;
using System.Collections;
using System.Collections.Generic;
using Presentation.Player;
using Signals.EventManager;
using UnityEngine;
using Utils;

public class LightManagerLevelTwo : MonoBehaviour
{
    [SerializeField] private List<Light> _lights;
    [SerializeField] private Light _lightsEndLevel;
    [SerializeField] private float _timeToSwitchOnLights;
    private IEventManager _eventManager;
    private int _lightsEnded = 0;

    private void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    void Start()
    {
        _lightsEndLevel.enabled = false;

        _eventManager.AddActionToSignal<SwitchOffAllLightsLevelTwoSignal>(SwitchOffAllLights);
    }

    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<SwitchOffAllLightsLevelTwoSignal>(SwitchOffAllLights);
    }

    private void SwitchOffAllLights(SwitchOffAllLightsLevelTwoSignal obj)
    {
        if (_lights == null)
        {
            return;
        }
        foreach (var light in _lights)
        {
            light.enabled = true;
            SwitcherLight switcherLight = new SwitcherLight(light, _timeToSwitchOnLights, true, 0);
            StartCoroutine(switcherLight.Start());
        }

        _lightsEndLevel.enabled = true;
    }
}