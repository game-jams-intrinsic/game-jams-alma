﻿using Presentation.Items;
using UnityEngine;
using Utils;

public class Piano : InteractableItem
{
    private bool _hasBeenTouchedBefore;
    [SerializeField] private ActionToDo _actionToDo;

    private void Start()
    {
        _hasBeenTouchedBefore = false;
        SetTextToShow(ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageText().TextToGetTouchPianoInGameKey);
    }

    public override void InteractWithObject()
    {
        if (_hasBeenTouchedBefore)
        {
            return;
        }
        new PlayAudioWhenPlayerTouchesPianoSignal().Execute();
        _hasBeenTouchedBefore = true;
    }

    public override void InteractWithObject(GameObject objectWhichInteract)
    {
        InteractWithObject();
    }
}