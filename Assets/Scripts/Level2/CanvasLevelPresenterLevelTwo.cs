﻿
public class CanvasLevelPresenterLevelTwo : CanvasLevelPresenter
{
    private FlashBackViewLevelTwo _flashBackViewLevelTwo;

    private void Awake()
    {
        base.Awake();
        _flashBackViewLevelTwo = _flashBackViewLevel.GetComponent<FlashBackViewLevelTwo>();

        _pieceImage.gameObject.SetActive(false);
    }

    private void Start()
    {
        base.Start();
        _eventManager.AddActionToSignal<ShowFlashBackWhenMirrorIsCompletedLevelTwoSignal>(ShowFlashBackWhenMirrorIsCompleted);
    }

    private void OnDestroy()
    {
        base.OnDestroy();
    }

    private void ShowFlashBackWhenMirrorIsCompleted(ShowFlashBackWhenMirrorIsCompletedLevelTwoSignal obj)
    {
        new ShowOnlyUIMaskSignal().Execute();
        HideGlassPiece();
        _flashBackViewLevelTwo.gameObject.SetActive(true);
        _flashBackViewLevelTwo.ShowFlashWhenMirrorIsCompleted();
    }
}